#!/bin/env bash
set -e

echo "Begin Django initialization..."

# Migrations should be created manually by developers and
# committed with the source code repo. Set the MAKE_MIGRATIONS
# env var to a non-empty string to create migration scripts
# after changes are made to the Django ORM models.
if [ -n "$MAKE_MIGRATIONS" ]; then
echo "Generating database migration scripts..."
python manage.py makemigrations --no-input
exit 0
fi

echo "Apply database migrations..."
python manage.py migrate
# echo "Provision database..."
# python manage.py createcachetable
echo "Collect static files..."
python manage.py collectstatic --no-input
echo "Initializing periodic tasks..."
python manage.py initialize_periodic_tasks

echo "Create Django superuser..."
while [[ "$SUCCESS" != "true" ]]; do
  regex=".*That username is already taken*"
  set +e
  ERR_MSG="$(python manage.py createsuperuser --no-input 2>&1 > /dev/null | grep -v "registering new views" )"
  set -e
  if [[ "$ERR_MSG" == "" ]]; then
    echo "superuser created successfully"
    SUCCESS="true"
  fi
  if [[ "$ERR_MSG" =~ $regex ]]; then
    echo "superuser already exists"
    SUCCESS="true"
  fi
  if [[ "$SUCCESS" == "true" ]]; then
    touch /tmp/superuser_created
  else
    echo "Error: $ERR_MSG"
    SUCCESS="false"
    sleep 2
  fi
done

echo "Django initialization complete."
