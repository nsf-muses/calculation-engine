#!/bin/env bash
set -e

cd ${CE_BASE_DIR}/app

set +e
while (! docker stats --no-stream 2>/dev/null 1>&2 ); do
    echo "Waiting for Docker service..."
    sleep 2
done
echo "Docker service is online."
set -e

bash ../run/wait-for-it.sh ${DATABASE_HOST}:${DATABASE_PORT:-5432} --timeout=0
bash ../run/wait-for-it.sh ${MESSAGE_BROKER_HOST}:${MESSAGE_BROKER_PORT:-5672} --timeout=0
bash ../run/wait-for-it.sh ${API_SERVER_HOST}:${API_SERVER_PORT} --timeout=0

if [[ $DOCKER_CONFIG_JSON != '' ]]; then
    mkdir -p /home/ce/.docker
    echo $DOCKER_CONFIG_JSON > /home/ce/.docker/config.json
fi

QUEUES=$1

if [[ $DEV_MODE == "true" ]]; then
    watchmedo auto-restart --directory=./ --pattern=*.py --recursive -- \
    celery -A calculation_engine worker \
        --queues $QUEUES \
        --loglevel ${CELERY_LOG_LEVEL:-DEBUG} \
        --concurrency ${CELERY_CONCURRENCY:-4}
else
    celery -A calculation_engine worker \
        --queues $QUEUES \
        --loglevel ${CELERY_LOG_LEVEL:-INFO} \
        --concurrency ${CELERY_CONCURRENCY:-4}
fi
