#!/bin/bash

set -e

cd "$(dirname "$(readlink -f "$0")")"

PROFILE=$1
ACTION=$2

COMPOSE_ARGS_UP="--build --remove-orphans"
COMPOSE_ARGS_DOWN="--remove-orphans"
COMPOSE_ARGS_LOGS="--follow"
COMPOSE_LOG_LIST="api-server celery-worker-api celery-worker-jobs"
COMPOSE_PROJECT_NAME=""
COMPOSE_FILE="docker-compose.yaml"
CE_ENV_FILE=".env.dev"

case "$PROFILE" in
  "dev")
    COMPOSE_ARGS_UP="${COMPOSE_ARGS_UP} -d"
    COMPOSE_PROJECT_NAME="ce-dev"
    ;;
  "jupyter")
    COMPOSE_ARGS_UP="${COMPOSE_ARGS_UP} -d"
    COMPOSE_PROJECT_NAME="ce-dev"
    COMPOSE_LOG_LIST="${COMPOSE_LOG_LIST} jupyterlab"
    ;;
  "test")
    CE_ENV_FILE=".env.test"
    COMPOSE_ARGS_UP="${COMPOSE_ARGS_UP} --exit-code-from test-runner"
    COMPOSE_PROJECT_NAME="ce-test"
    ;;
  "docs_dev" | "docs_prod")
    COMPOSE_FILE="docker-compose.docs.yaml"
    COMPOSE_LOG_LIST="sphinx"
    COMPOSE_PROJECT_NAME="ce-docs"
    ;;
  *)
    echo "ERROR: You must specify a valid profile (e.g. $(basename $0) dev)"
    exit 1
    ;;
esac

case "$PROFILE" in
  "docs_dev")
    PRECMD_UP="export UID; mkdir -p /tmp/ce_docs/{module_clones,module_docs,sphinx_build};"
    ;;
  *)
    PRECMD_UP=""
    ENV_FILES="--env-file ${CE_ENV_FILE}"
    # Create extra env var file if it does not exist
    if [[ ! -f "${CE_ENV_FILE}" ]]; then
      touch "${CE_ENV_FILE}"
    fi
    ;;
esac

case "$ACTION" in
  "up")
    set -x
    eval "${PRECMD_UP}"
    docker compose \
        --file ${COMPOSE_FILE} \
        ${ENV_FILES} \
        --profile ${PROFILE} \
        --project-name ${COMPOSE_PROJECT_NAME} \
        up \
        ${COMPOSE_ARGS_UP}
    set +x
    ;;
  "down")
    set -x
    docker compose \
        --file ${COMPOSE_FILE} \
        ${ENV_FILES} \
        --profile ${PROFILE} \
        --project-name ${COMPOSE_PROJECT_NAME} \
        down \
        ${COMPOSE_ARGS_DOWN}
    set +x
    ;;
  "logs")
    set -x
    docker compose \
        --file ${COMPOSE_FILE} \
        ${ENV_FILES} \
        --profile ${PROFILE} \
        --project-name ${COMPOSE_PROJECT_NAME} \
        logs \
        ${COMPOSE_ARGS_LOGS} \
        ${COMPOSE_LOG_LIST}
    set +x
    ;;
  "destroy-volumes")
    set -x
    docker compose \
        --file ${COMPOSE_FILE} \
        ${ENV_FILES} \
        --profile ${PROFILE} \
        --project-name ${COMPOSE_PROJECT_NAME} \
        down \
        ${COMPOSE_ARGS_DOWN} \
        --volumes
    set +x
    ;;
  *)
    echo "ERROR: You must specify a valid action (e.g. $(basename $0) up)"
    exit 1
    ;;
esac
