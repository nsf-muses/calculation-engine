import sys
import logging
from calculation_engine_api import CalculationEngineApi
logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    api = CalculationEngineApi()

    '''
    openssl rand -out /tmp/test.1GB.dat -base64 $(( 1 * 10**9 ))
    openssl rand -out /tmp/test.10MB.dat -base64 $(( 10 * 10**6 ))
    '''

    uploads = api.upload_list()
    for dummy_data in [
        'test.10MB.dat',
        # 'test.750MB.dat',
        'test.1GB.dat',
    ]:
        upload_response = api.upload_file(
            file_path=f'/tmp/{dummy_data}',
            upload_path=dummy_data,
            description=dummy_data,
            public=True,
        )
    uploads = api.upload_list()

    for upload in uploads:
        api.delete_uploaded_file(uuid=upload['uuid'])
    uploads = api.upload_list()
