import os
import yaml
import sys
from pathlib import Path
import logging
logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
logger = logging.getLogger(__name__)

# Append the calculation_engine_api module to the Python path for import
sys.path.append(os.path.join(str(Path(__file__).resolve().parent.parent), 'app'))
from calculation_engine.tests.calculation_engine_api import CalculationEngineApi

if __name__ == "__main__":
    api = CalculationEngineApi()
    metrics = api.get_metrics()
    print(metrics)
    with open('/tmp/metrics.yaml', 'w') as fh:
        yaml.dump(metrics, fh)
