import os
import sys
import yaml
import json
import requests
from git import Repo
import pathlib
import shutil
from datetime import datetime, timezone
import subprocess

CONFIG_FILEPATH = 'config/config.yaml'
CFF_FILEPATH = 'CITATION.cff'
BIBTEX_FILEPATH = 'docs/src/user/policies/bibtex.bib'
EXPECTED_PATHS = [
    CONFIG_FILEPATH,
    CFF_FILEPATH,
    BIBTEX_FILEPATH,
]


def get_git_repo():
    repo = Repo(pathlib.Path(__file__).parent.parent.resolve())
    return repo


def git_tag_exists(version):
    repo = get_git_repo()
    tag = f'v{version}'
    return tag in repo.tags


def check_git_branch_and_tag(version, testing=False):
    repo = get_git_repo()
    tag = f'v{version}'
    if git_tag_exists(version):
        print(f'Tag {tag} already exists. Aborting...')
        sys.exit(1)
    # print(repo.active_branch.name)
    if not testing and repo.active_branch.name != 'main':
        print('You must be on the main branch. Aborting...')
        sys.exit(1)
    return True


def git_commit_and_tag(version):
    repo = get_git_repo()
    tag = f'v{version}'
    # Verify validity of repo state
    assert check_git_branch_and_tag(version)
    # Ensure that only the expected files are staged for commit
    staged_items = [item.a_path for item in repo.index.diff('HEAD')]
    unexpected_paths = [path for path in staged_items if path not in EXPECTED_PATHS]
    if unexpected_paths:
        print(f'''Aborting. Unexpected items are staged: {unexpected_paths}''')
        sys.exit()
    if not staged_items:
        print('''No items are staged for commit. Aborting...''')
        sys.exit()
    print(f'''Items staged for commit: {staged_items}''')
    print('Committing to main branch...')
    repo.index.commit(f'Release version {tag}')
    print(f'Tagging commit "{tag}"...')
    repo.create_tag(tag)


def generate_bibtex_file():
    proc = subprocess.Popen(['/bin/bash', '-c',
                             ('''docker run --rm -v $PWD:/app citationcff/cffconvert -f bibtex '''
                              '''| sed 's/YourReferenceHere/MusesCalculationEngine/g' '''
                              f'''> {BIBTEX_FILEPATH}''')
                             ])
    try:
        outs, errs = proc.communicate(timeout=1800)
        # Stage the changes
        repo = get_git_repo()
        repo.index.add([BIBTEX_FILEPATH])
    except Exception as e:
        proc.kill()
        outs, errs = proc.communicate()
        print(f'''Error:\n{e}\n{outs}\n{errs}''')
        raise


def update_citation_file(record_info):
    # Update version in CFF file
    with open(CFF_FILEPATH) as fp:
        cff_data = yaml.safe_load(fp)
    original_version = cff_data['version']
    original_doi = cff_data['identifiers'][0]['value']
    cff_data['version'] = record_info['metadata']['version']
    cff_data['identifiers'][0]['value'] = record_info['metadata']['prereserve_doi']['doi']
    # If neither the version nor DOI needs to be updated, do not update the file
    if original_version == cff_data['version'] and original_doi == cff_data['identifiers'][0]['value']:
        print('No updates to CITATION.cff are necessary.')
        return
    # Update the metadata and update the file
    cff_data['date-released'] = datetime.now(timezone.utc).strftime("%Y-%m-%d")
    with open(CFF_FILEPATH, 'w') as fp:
        yaml.dump(cff_data, fp, sort_keys=False)
    # Stage the changes
    repo = get_git_repo()
    repo.index.add([CFF_FILEPATH])


def update_config(record_info):
    version = record_info['metadata']['version']
    # Update version in CE config file
    
    with open(CONFIG_FILEPATH) as fp:
        config_data = yaml.safe_load(fp)
    original_version = config_data['version']
    config_data['version'] = version
    # If the version does not need to be updated, do not update the file
    if original_version == config_data['version']:
        print('No updates to the CE config are necessary.')
        return
    # Update the config and update the file
    with open(CONFIG_FILEPATH, 'w') as fp:
        yaml.dump(config_data, fp, sort_keys=False)
    # Stage the changes
    repo = get_git_repo()
    repo.index.add([CONFIG_FILEPATH])


def zenodo_get_record_title(version):
    return f'MUSES Calculation Engine v{version}'


def zenodo_get_record_filepath(version):
    return f'/var/tmp/zenodo_record.{version}.json'


def zenodo_get_archive_filepath(version):
    return f'/tmp/ce.v{version}.zip'


def create_release_archive_file(version):
    rev = f'''v{version}'''
    clone_dir = f'/tmp/ce/{rev}'
    if os.path.isfile(zenodo_get_archive_filepath(version)):
        print(f'Archive file already exists: "{zenodo_get_archive_filepath(version)}"')
        return
    # Clone the local repo and checkout the target version.
    # This ensures a pristine copy unmarred by local
    # modifications or private files.
    src_dir = pathlib.Path(__file__).parent.parent.resolve()
    print(f'Cloning repo to "{clone_dir}"...')
    repo = Repo.clone_from(src_dir, clone_dir)
    repo.git.checkout(rev)
    # Remove the unnecessary .git folder before zipping
    shutil.rmtree(os.path.join(clone_dir, '.git'))
    print(f'Making archive file "{zenodo_get_archive_filepath(version)}"...')
    shutil.make_archive(zenodo_get_archive_filepath(version).replace('.zip', ''), 'zip', clone_dir)


def zenodo_get_upload(record_info):
    response = requests.get(
        f'''https://zenodo.org/api/deposit/depositions/{record_info['id']}/files''',
        params={
            'access_token': os.getenv('ZENODO_ACCESS_TOKEN', ''),
        },
    )
    files = response.json()
    print(files)
    archive_filepath = zenodo_get_archive_filepath(version)
    target_filename = os.path.basename(archive_filepath)
    file = [file for file in files if file['filename'] == target_filename]
    if file:
        file_info = file[0]
        print(f'''Archive file already uploaded:\n{json.dumps(file_info, indent=2)}''')
        return file_info
    else:
        return {}


def zenodo_get_bucket_id(record_info):
    response = requests.get(
        f'''https://zenodo.org/api/deposit/depositions/{record_info['id']}''',
        params={
            'access_token': os.getenv('ZENODO_ACCESS_TOKEN', ''),
        },
    )
    print(response.json())
    bucket_url = response.json()["links"]["bucket"]
    return bucket_url


def zenodo_upload_file(record_info):
    archive_filepath = zenodo_get_archive_filepath(version)
    target_filename = os.path.basename(archive_filepath)
    bucket_url = zenodo_get_bucket_id(record_info)
    with open(archive_filepath, "rb") as fp:
        response = requests.put(
            os.path.join(bucket_url, target_filename),
            data=fp,
            params={
                'access_token': os.getenv('ZENODO_ACCESS_TOKEN', ''),
            },
        )
    print(f'''[{response.status_code}]\n'''
          f'''{json.dumps(response.json(), indent=2)}''')


def zenodo_get_record(version):
    zenodo_record_file = zenodo_get_record_filepath(version)
    # Ref: https://www.elastic.co/guide/en/elasticsearch/reference/current/
    #      query-dsl-query-string-query.html#query-string-syntax
    response = requests.get(
        'https://zenodo.org/api/deposit/depositions',
        params={
            'q': f'title:"{zenodo_get_record_title(version)}"',
            'access_token': os.getenv('ZENODO_ACCESS_TOKEN', ''),
        },
    )
    results = response.json()
    if results:
        print(f'''Downloading existing Zenodo record info into "{zenodo_record_file}".''')
        # Store Zenodo record info as file (assuming that the title is unique)
        record_info = results[0]
        with open(zenodo_record_file, 'w') as fp:
            json.dump(record_info, fp, indent=2)
        return record_info
    return {}


def zenodo_new_record(version):
    '''Refs: https://developers.zenodo.org/#create, https://developers.zenodo.org/#representation'''
    # If a Zenodo record has already been created for this version
    # do not create a new one.
    zenodo_record_file = zenodo_get_record_filepath(version)
    print(f'Creating new Zenodo record for version "{version}"...')
    metadata = {
        'upload_type': 'software',
        'title': zenodo_get_record_title(version),
        'description': (
            'The MUSES Calculation Engine is a web-based application and workflow management system '
            'allowing researchers to run scientific calculations as composable workflows, constructed '
            'from a growing library of MUSES modules that calculate equations of state, '
            'derive physical observables, and transform data. MUSES is an NSF-funded project to answer '
            'critical interdisciplinary questions in nuclear physics, gravitational wave astrophysics, '
            'and heavy-ion physics.'
        ),
        'version': str(version),
        'license': 'mpl-2.0',
        'creators': [{
            'name': 'Manning, T. Andrew',
            'affiliation': ('National Center for Supercomputing Applications, '
                            'University of Illinois Urbana-Champaign, Urbana, IL, USA'),
            'orcid': '0000-0003-2545-9195',
        }],
        'custom': {
            'code:codeRepository': 'https://gitlab.com/nsf-muses/calculation-engine',
            'code:programmingLanguage': [
                {
                    'id': 'python',
                    'title': {
                        'en': 'Python'
                    }
                }
            ],
            'code:developmentStatus': {
                'id': 'active',
                'title': {
                    'en': 'Active'
                }
            }
        },
        'keywords': [
            'nuclear physics',
            'astrophysics',
            'heavy-ion physics',
            'workflow management system',
            'science gateway',
        ],
        'references': [
            ('Manning, T. A. (2024). MUSES Calculation Engine. GitLab.com. '
                'https://gitlab.com/nsf-muses/calculation-engine'),
            ('MUSES Collaboration. (n.d.). Modular Unified Solver of the Equation of State. '
                'https://musesframework.io'),
        ],
        'grants': [
            # National Science Foundation: 10.13039/100000001
            {'id': '10.13039/100000001::2103680'}
        ],
        'communities': [
            {'identifier': 'nsf-muses'}
        ],
        'prereserve_doi': True,
        'language': 'eng',
    }
    response = requests.post(
        'https://zenodo.org/api/deposit/depositions',
        params={'access_token': os.getenv('ZENODO_ACCESS_TOKEN', '')},
        json={'metadata': metadata}
    )
    record_info = response.json()
    if response.status_code in range(200, 300):
        # Store Zenodo record info as file
        with open(zenodo_record_file, 'w') as fp:
            json.dump(record_info, fp, indent=2)
        return record_info
    else:
        print(f'''[{response.status_code}]\n'''
              f'''{json.dumps(record_info, indent=2)}''')
        return None


if __name__ == "__main__":

    # Set target version from command line argument
    version = sys.argv[1]
    testing = False
    if len(sys.argv) > 2:
        testing = sys.argv[2] == 'testing'

    # First create/fetch the draft Zenodo record to obtain the prereserved DOI
    record_info = zenodo_get_record(version)
    if not record_info:
        print('Zenodo record not found. Creating new record...')
        record_info = zenodo_new_record(version)
    if not record_info:
        print('Error creating Zenodo record. Aborting.')
        sys.exit()

    # Verify validity of local Git state
    assert check_git_branch_and_tag(version, testing=testing)

    if not git_tag_exists(version):
        print(f'Creating tagged release "v{version}"...')
        # Update the CE config file
        update_config(record_info)
        # Update the CFF file with the prereserved DOI and new version
        update_citation_file(record_info)
        generate_bibtex_file()
        if testing:
            sys.exit()
        # Commit the updated files and tag the commit
        git_commit_and_tag(version)

    # If the archive file has not already been uploaded
    if not zenodo_get_upload(record_info):
        create_release_archive_file(version)
        zenodo_upload_file(record_info)
