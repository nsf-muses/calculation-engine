import requests
import time
from calculation_engine.log import get_logger
from django.conf import settings
log = get_logger(__name__)

# Set a default wait duration in seconds to avoid Discourse API rate limits
default_wait_duration = 10


class DiscourseApi():
    # ref: https://docs.discourse.org/
    def __init__(self):
        self.api_user = settings.DISCOURSE_API_USER
        self.api_key = settings.DISCOURSE_API_KEY
        self.base_url = settings.DISCOURSE_API_BASEURL

    def discourse_get_user_info(self, user_id='', email=''):
        log.debug('Running discourse_get_user_info()...')
        assert user_id or email
        if user_id:
            url = f'''{self.base_url}/admin/users/{user_id}.json'''
        else:
            url = f'''{self.base_url}/admin/users/list/active.json?email={email}'''
        r = requests.request('GET',
                             url,
                             headers={
                                 'Api-Username': self.api_user,
                                 'Api-Key': self.api_key,
                             })
        try:
            assert r.status_code == 200
            response = r.json()
            if email and len(response) > 0:
                response = response[0]
        except Exception as err:
            log.error(err)
            if r.status_code == 429:
                log.debug(f'''discourse_get_user_info response text: {r.text}''')
                time.sleep(default_wait_duration)
                return self.discourse_get_user_info(user_id=user_id, email=email)
            log.error(r.status_code)
            log.error(r.text)
            response = r
        return response

    def user_in_group(self, email='', group_name=''):
        user_entry = self.discourse_get_user_info(email=email)
        if not user_entry:
            log.warning(f'''No Discourse user associated with email "{email}".''')
            return False
        user_info = self.discourse_get_user_info(user_id=user_entry['id'])
        if [grp for grp in user_info['groups'] if grp['name'] == group_name]:
            log.debug(f'''Discourse user "{email}" already in group "{group_name}"''')
            return True
        else:
            return False

    # def construct_post(self, user_info):
    #     email = user_info['email']
    #     username = user_info['username']
    #     message_body = (f'''New CE user: "{user_info['first_name']} {user_info['last_name']}", '''
    #                     f'''{username}, {email}\n\n''')
    #     message_body_html = f'''
    #         <h2>New CE user access request</h2>
    #         <p>
    #             name: {user_info['first_name']} {user_info['last_name']}<br />
    #             username: <code>{username}</code><br />
    #             email: <code>{email}</code>
    #         </p>
    #         <p>
    #             Approval will add the user to the "ce" Keycloak group and the 
    #             <a href="https://forum.musesframework.io/g/ce-users">"ce-users" Discourse group</a>
    #             (if they are not a collaborator).
    #         </p>
    #     '''.replace('\n            ', '')
    #     # Generate hash of email address using secret salt
    #     from urllib import parse
    #     import bcrypt
    #     email_hash = bcrypt.hashpw(bytes(email, 'utf-8'), bytes(settings.SHARED_HASH_SALT, "utf-8"))
    #     email_hash_urlencoded = parse.quote_plus(email_hash)
    #     email_urlencoded = parse.quote_plus(email)
    #     approve_link = (f'''https://musesframework.io/api/v1/register/ce'''
    #                     f'''/approve/{email_urlencoded}?token={email_hash_urlencoded}''')
    #     deny_link = (f'''https://musesframework.io/api/v1/register/ce'''
    #                  f'''/reject/{email_urlencoded}?token={email_hash_urlencoded}''')
    #     message_body += f'''[✅ Click here to APPROVE the request]({approve_link})\n\n'''
    #     message_body += f'''[🚫 Click here to DENY the request]({deny_link})\n\n'''
    #     message_body_html += f'''
    #         <hr>
    #             <p><a href="{approve_link}">✅ Click here to APPROVE the request.</a></p>
    #         <hr>
    #             <p><a href="{deny_link}">🚫 Click here to DENY the request.</a></p>
    #         <hr>
    #     '''.replace('\n            ', '')
    #     # logger.debug(f'''{message_body}''')
    #     log.debug(f'''{message_body_html}''')
    #     return message_body_html
