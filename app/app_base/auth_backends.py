# This auth backend subclass fixes a bug related to the error:
#   OIDC callback state not found in session `oidc_states`!
# ref: https://github.com/mozilla/mozilla-django-oidc/issues/435


from mozilla_django_oidc.auth import OIDCAuthenticationBackend
from django.contrib.auth.models import Group
import unicodedata
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from app_base.discourse import DiscourseApi

from calculation_engine.log import get_logger
logger = get_logger(__name__)


class KeycloakOIDCAuthenticationBackend(OIDCAuthenticationBackend):
    def __init__(self):
        OIDCAuthenticationBackend.__init__(self)

    def update_groups(self, user, claims):
        # Map the OIDC group memberships to local groups
        for oidc_group, group_name in settings.GROUP_MAP.items():
            if is_member_of(claims, oidc_group):
                # Create local groups if they do not exist
                group, _ = Group.objects.get_or_create(name=group_name)
                user.groups.add(group)

    def user_is_authorized(self, email):
        # Check membership in Discourse groups
        api = DiscourseApi()
        return api.user_in_group(email=email, group_name=settings.DISCOURSE_CE_GROUP_NAME)

    def create_user(self, claims):
        """ Overrides Authentication Backend so that Django users are
            created with the keycloak preferred_username.
            If nothing found matching the email, then try the username.
        """
        logger.debug(f'KeycloakOIDCAuthenticationBackend.create_user claims: {claims}')
        email = self.get_email(claims)
        user_info = {
            'username': claims.get('preferred_username'),
            'email': email,
            # 'is_staff': is_member_of(claims, settings.STAFF_GROUP),
            'is_staff': self.user_is_authorized(email),
            'is_superuser': is_member_of(claims, settings.ADMIN_GROUP),
            'first_name': claims.get('given_name', ''),
            'last_name': claims.get('family_name', ''),
        }
        new_user = self.UserModel.objects.create(
            username=user_info['username'],
            email=user_info['email'],
            is_staff=user_info['is_staff'],
            is_superuser=user_info['is_superuser'],
            first_name=user_info['first_name'],
            last_name=user_info['last_name'],
        )
        self.update_groups(new_user, claims)
        logger.debug(f'''user: {new_user}''')
        return new_user

    def filter_users_by_claims(self, claims):
        """ Return all users matching the specified email.
            If nothing found matching the email, then try the username
        """
        email = claims.get('email')

        if not email:
            return self.UserModel.objects.none()
        users = self.UserModel.objects.filter(email__iexact=email)
        return users

    def update_user(self, user, claims):
        email = claims.get('email')
        user.first_name = claims.get('given_name', '')
        user.last_name = claims.get('family_name', '')
        user.email = email
        # user.is_staff = is_member_of(claims, settings.STAFF_GROUP)
        user.is_staff = self.user_is_authorized(email)
        user.is_superuser = is_member_of(claims, settings.ADMIN_GROUP)
        user.save()
        self.update_groups(user, claims)
        return user

    def get_email(self, claims):
        """Extract email from claims"""
        email = ""
        if "email" in claims:
            email = claims.get("email")
        elif "email_list" in claims:
            email = claims.get("email_list")

        if isinstance(email, list):
            email = email[0]
        return email


def generate_username(email):
    # Using Python 3 and Django 1.11+, usernames can contain alphanumeric
    # (ascii and unicode), _, @, +, . and - characters. So we normalize
    # it and slice at 150 characters.
    return unicodedata.normalize('NFKC', email)[:150]


def execute_logout(request):
    """This implements the OIDC_OP_LOGOUT_URL_METHOD from Django settings. It is
    currently a placeholder.

    NOTES:
      * must return the logout URL
      * called as a hook (via settings.OIDC_OP_LOGOUT_URL_METHOD) from
        mozilla_django_oidc.OIDCLogoutView.post() (from /logout endpoint)
      * the request.user is a django.utils.functional.SimpleLazyObject which is a wrapper
        around a django.contrib.auth.User (see SO:10506766).
    """

    try:
        request.user.auth_token.delete()
    except (AttributeError, ObjectDoesNotExist):
        pass

    return settings.LOGOUT_REDIRECT_URL


def is_member_of(claims, group):
    return group in claims.get('groups', [])
