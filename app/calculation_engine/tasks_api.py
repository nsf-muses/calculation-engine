from celery import shared_task
import os
import time
from celery.result import AsyncResult
from calculation_engine.celery import app
from calculation_engine.models import Job
from calculation_engine.object_store import ObjectStore
from calculation_engine.log import get_logger
from django.conf import settings
logger = get_logger(__name__)

s3 = ObjectStore()


task_time_limit = int(os.environ.get("TASK_TIME_LIMIT", "3800"))
task_soft_time_limit = int(os.environ.get("TASK_SOFT_TIME_LIMIT", "3600"))


@shared_task(name='Revoke Job',
             time_limit=task_time_limit,
             soft_time_limit=task_soft_time_limit)
def revoke_job(job_id):
    try:
        # Terminate all workflow tasks
        logger.info(f'''Revoking job "{job_id}"...''')
        job = Job.objects.get(uuid__exact=job_id)
        logger.info(f'''Revoking tasks: {job.task_ids}''')
        # NOTE: Although the documentation says you can supply a list of
        #       task IDs to the terminate/revoke functions, empirically
        #       it only works when you terminate tasks one-by-one.
        app.control.terminate(job_id, signal='SIGKILL')
        for task_id in job.task_ids:
            app.control.terminate(task_id, signal='SIGKILL')

        time_start = time.time()
        timeout = 120  # seconds
        wait = True
        active_job_states = ['STARTED', 'RETRY', 'RECEIVED']
        while timeout > time.time() - time_start and wait:
            wait = False
            for task_id in job.task_ids:
                result = AsyncResult(task_id)
                if result and result.status in active_job_states:
                    logger.info(f'''Waiting for task to stop: {task_id}''')
                    wait = True
            time.sleep(5)
        for task_id in job.task_ids:
            result = AsyncResult(task_id)
            if result and result.status in active_job_states:
                logger.info(f'''Ignoring task {task_id} still in state "{result.status}".''')
    except Exception as err:
        logger.error(f'''Error attempting to terminate job "{job_id}": {err}''')


@shared_task(name='Delete Job Files',
             time_limit=task_time_limit,
             soft_time_limit=task_soft_time_limit)
def delete_job_files(job_id):
    job_dir = os.path.join(settings.S3_BASE_DIR, 'jobs', job_id)
    s3.delete_directory(job_dir)


@shared_task(name='Delete Job',
             time_limit=task_time_limit,
             soft_time_limit=task_soft_time_limit)
def delete_job(job_id):
    job = Job.objects.get(uuid__exact=job_id)
    job.delete()
