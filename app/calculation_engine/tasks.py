from celery import shared_task
import os
import time
import json
import yaml
import docker
from celery.result import AsyncResult
from calculation_engine.models import update_job_state, update_workflow_state
from celery.signals import task_failure
from celery.signals import task_postrun
from celery.signals import task_revoked

from celery.exceptions import SoftTimeLimitExceeded
from django.core.exceptions import ObjectDoesNotExist
from calculation_engine.models import Job, Upload, JobFile
from calculation_engine.object_store import ObjectStore
from calculation_engine.log import get_logger
from calculation_engine.config import load_config_from_file
from pathlib import Path
import stat
from django.conf import settings
from shutil import rmtree
import datetime
import hashlib
logger = get_logger(__name__)

s3 = ObjectStore()


task_time_limit = int(os.environ.get("TASK_TIME_LIMIT", "3800"))
task_soft_time_limit = int(os.environ.get("TASK_SOFT_TIME_LIMIT", "3600"))


def validate_inputs(inputs={}, job_id=None):
    '''Validate list of inputs, returning empty string for valid spec or
    the specific error if invalid.'''
    try:
        job = Job.objects.get(uuid__exact=job_id)
        # Get current job object to obtain user info
    except ObjectDoesNotExist:
        return f'Job {job_id} not found.'
    # Iterate over inputs and validate permissions and existence
    for label, input in inputs.items():
        uuid = input['uuid']
        if input['type'] == 'upload':
            # If upload record exists, assume the file is in the storage location
            try:
                upload = Upload.objects.get(uuid__exact=uuid)
                assert upload.owner == job.owner or upload.public
            except (ObjectDoesNotExist, AssertionError):
                return f'Input error: upload uuid not found or is private. Label: {label}. Upload {uuid}'
        if input['type'] == 'job':
            # If saved job record exists, verify the file is in the specified location
            try:
                # TODO: Should we require that the referenced job is saved, or
                #       leave as is to allow the files to be purged after expiration?
                # savedjob = Job.objects.get(uuid__exact=uuid, saved__exact=True)
                savedjob = Job.objects.get(uuid__exact=uuid)
                assert savedjob.owner == job.owner or savedjob.public
            except (ObjectDoesNotExist, AssertionError):
                return f'Input error: saved job not found or is private. Label: {label}. Saved job {uuid}'
            path = input['path']
            job_file_path = os.path.join(
                settings.S3_BASE_DIR,
                'jobs',
                uuid,
                path.strip('/'),
            )
            try:
                assert s3.object_exists(path=job_file_path)
            except (ObjectDoesNotExist, AssertionError):
                return ("Input error: saved job file not found at specified path. "
                        f"Label: {label}, Saved job: {uuid}, Path: {path}")
    return ''


def initialize_io_dir(dir_path=''):
    os.makedirs(dir_path, exist_ok=True)
    # Initialize the path with open permissions so the module container can write files regardless of UID
    os.chmod(dir_path, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
    logger.debug(f'''dir stat for "{dir_path}": {os.stat(dir_path)}''')


def create_mounted_volumes(module_bindings={}, module_name='', config={}):
    # Construct Docker bind mounts for each input/output file
    mounted_volumes = []
    for label, binding in module_bindings.items():
        logger.debug(f'''Binding "{label}":\n{yaml.dump(binding)}''')
        # Mount input files read-only
        # read_only = binding['type'] == 'input'
        read_only = False
        docker_volume = docker.types.Mount(binding['container'], binding['host'], type='bind', read_only=read_only)
        if docker_volume not in mounted_volumes:
            mounted_volumes.append(docker_volume)
        else:
            logger.debug(f'''Redundant volume skipped: "{docker_volume}"''')
    if not module_name or not config:
        return mounted_volumes
    # Construct Docker bind mounts for any explicitly specified volumes
    try:
        explicit_volumes = [mod['volumes'] for mod in config['modules'] if mod['name'] == module_name]
    except KeyError:
        explicit_volumes = []
    if module_name and explicit_volumes:
        for binding in explicit_volumes[0]:
            host_path = binding['host']
            if not os.path.isabs(host_path):
                host_path = os.path.join(config['server']['fileBasePath'], host_path)
            docker_volume = docker.types.Mount(binding['container'], host_path,
                                               type='bind', read_only=binding['readOnly'])
            if docker_volume not in mounted_volumes:
                mounted_volumes.append(docker_volume)
            else:
                logger.debug(f'''Redundant volume skipped: "{docker_volume}"''')
    return mounted_volumes


@shared_task(bind=True, name='Run Module',
             time_limit=task_time_limit,
             soft_time_limit=task_soft_time_limit)
def run_module(
        self,
        job_id='',
        module_name="",
        process_name="",
        config={},
        pipes={},
        inputs={},
        sleep_time=0):
    logs = []
    task_status = {}
    error_messages = [] 

    s3_basepath = os.path.join(
        settings.S3_BASE_DIR,
        f'''jobs/{job_id}/{process_name}''')
    # Record task as active for monitoring
    update_workflow_state(job_id, process_names=[process_name], status='active')
    try:
        # Validate inputs spec, ensuring that the files exist and that
        # the current job owner has permission to access the files.
        invalid_msg = validate_inputs(inputs=inputs, job_id=job_id)
        assert not invalid_msg
    except AssertionError:
        error_messages.append(f'Invalid inputs spec: "{invalid_msg}"')
        logger.error(error_messages[-1])
        update_job_state(job_id, Job.JobStatus.FAILURE, error_info=error_messages[-1])
        raise Exception(error_messages[-1])
    try:
        logs.append((f'Job "{job_id}": Celery task "{self.request.id}" triggered for '
                     f'task "{process_name}" running module "{module_name}".'))
        # Artificially extend duration for testing purposes
        time.sleep(sleep_time)
        # Load CE config
        ce_config = load_config_from_file()
        module_config = [mod for mod in ce_config['modules'] if mod['name'] == module_name][0]
        logger.debug(f'module config: {yaml.dump(module_config)}')
        # Construct the container image URL
        image_info = module_config['image']
        image_tag = image_info['tag']
        if not image_info['registry']:
            image_reg_repo = f'''{image_info['repo']}'''
        else:
            image_reg_repo = f'''{image_info['registry']}/{image_info['repo']}'''
        image_url = f'''{image_reg_repo}:{image_tag}'''
        # Mount IO volumes
        bindings = {}
        task_basepath = os.path.join('/scratch', job_id, process_name)
        initialize_io_dir(task_basepath)
        for io in module_config['inputs']:
            io_label = io['label']
            io_host_filepath = os.path.join(task_basepath, 'inputs', io_label)
            io_host_dir = Path(io_host_filepath).parent
            initialize_io_dir(io_host_dir)
            # Initialize container volume binding for the input file
            bindings[io_label] = {
                'type': 'input',
                'host': io_host_filepath,
                'container': io['path'],
            }
            # If input is the config, write the input config data to disk
            if io_label == 'config':
                logger.debug(f'Writing config file "{io_host_filepath}"...')
                with open(io_host_filepath, 'w') as task_config_file:
                    yaml.dump(config, task_config_file)
                # os.chmod(io_host_filepath, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
                with open(io_host_filepath, 'r') as task_config_file:
                    logger.debug(f'Config file contents:\n"{yaml.load(task_config_file, Loader=yaml.FullLoader)}"')
            # If input is not the config, download the file from the object store
            else:
                pipe = [pipes[label] for label in pipes if label == io_label]
                file = [inputs[label] for label in inputs if label == io_label]
                if pipe:
                    pipe = pipe[0]
                    logger.debug(f'''pipe: {pipe}''')
                    prev_module_config = [
                        mod for mod in ce_config['modules'] if mod['name'] == pipe['module']][0]
                    # logger.debug(f'prev_module_config: {yaml.dump(prev_module_config)}')
                    piped_output = [out for out in prev_module_config['outputs'] if out['label'] == pipe['label']][0]
                    # logger.debug(f'piped_output: {piped_output}')
                    piped_output_s3_path = os.path.join(
                        settings.S3_BASE_DIR,
                        'jobs',
                        job_id,
                        pipe['process'],
                        piped_output['path'].strip('/'),
                    )
                    s3.download_object(
                        path=piped_output_s3_path,
                        file_path=io_host_filepath,
                    )
                elif file:
                    file = file[0]
                    logger.debug(f'''file: {file}''')
                    uuid = file['uuid']
                    if file['type'] == 'upload':
                        upload = Upload.objects.filter(uuid__exact=uuid)
                        if not upload:
                            error_messages.append(f"Error in module {module_name}: Cannot find upload with "
                                                  f"uuid {file['uuid']}. Label: {io_label}")
                            raise AssertionError(error_messages[-1])
                        upload = upload[0]
                        try:
                            assert 'checksum' in file
                            assert isinstance(file['checksum'], str)
                        except AssertionError:
                            logger.error('''Aborting workflow. Invalid or missing upload checksum value.''')
                            error_messages.append(f"Error in module {module_name}: Invalid or missing upload checksum "
                                                  f"file labeled {io_label} with uuid {file['uuid']}")
                            raise
                        checksum = file['checksum']
                        with upload.file.open(mode='rb') as uploaded_file, open(io_host_filepath, 'wb') as input_file:
                            # Calculate md5sum and error if it does not match the declared value
                            data = uploaded_file.read()
                            md5sum = hashlib.md5(data).hexdigest()
                            logger.debug(f'''checksum: {checksum}, md5sum: {md5sum}''')
                            try:
                                assert checksum == md5sum
                            except AssertionError:
                                logger.error('''Aborting workflow. File integrity check failed: checksum mismatch.''')
                                error_messages.append(f"Error in module {module_name}: Checksum mismatch for "
                                                      f"file labeled {io_label} with uuid {file['uuid']}")
                                raise
                            input_file.write(data)
                    elif file['type'] == 'job':
                        path = file['path']
                        job_file_path = os.path.join(
                            settings.S3_BASE_DIR,
                            'jobs',
                            uuid,
                            path.strip('/'),
                        )
                        s3.download_object(
                            path=job_file_path,
                            file_path=io_host_filepath,
                        )
                else:
                    # If neither pipe nor existing file is specified, verify that the input is not required.
                    # If the `required` key is absent, assume the input/output is required.
                    io_required = io['required'] if 'required' in io else True
                    if io_required:
                        err_msg = f'"Error in module {module_name}: Required input is missing: "{io_label}"'
                        logger.error(err_msg)
                        error_messages.append(err_msg)
                        raise Exception(err_msg)
                    else:
                        bindings.pop(io_label)

        for io in module_config['outputs']:
            io_label = io['label']
            container_dir_path = f'''{Path(io['path']).parent}'''
            host_dir_path = os.path.join(task_basepath, 'outputs', container_dir_path.strip('/'))
            initialize_io_dir(host_dir_path)
            bindings[io_label] = {
                'type': 'output',
                'host': host_dir_path,
                'container': container_dir_path,
            }
            if io_label == 'status':
                status_filepath = os.path.join(host_dir_path, Path(io['path']).name)
        mounted_volumes = create_mounted_volumes(module_bindings=bindings)

        # Obtain Docker client and initialize Docker environment
        client = docker.from_env()
        if image_tag in ['latest', 'dev']:
            client.images.pull(image_reg_repo, tag=image_tag)
        # Run container
        container_name = f'''{job_id}-{process_name}-{self.request.id}'''
        container = client.containers.run(
            name=container_name,
            image=image_url,
            detach=True,
            # auto_remove=True,
            mounts=mounted_volumes,
            command=module_config['command'],
        )
        # For debugging purposes, list the current containers
        # TODO: Move this to a periodic task that prunes obsolete containers
        try:
            container_list = [container.name for container in client.containers.list(all=True)]
            logger.info(f'''List of containers: {json.dumps(container_list, indent=2)}''')
        except Exception as err:
            logger.warning(f'''Error listing Docker containers: {err}''')
        # try:
        #     client.containers.prune()
        # except Exception as err:
        #     logger.warning(f'''Error pruning Docker containers: {err}''')
        # Fetch container logs in real-time
        logger.info(f'''Log for container "{container.name}":''')
        for line in container.logs(stream=True):
            logs.append(line.strip().decode('utf-8'))
            logger.info(logs[-1])
        # Pause execution until container stops
        container.wait()
        container.remove(force=True)
        # Fetch the task reported status
        with open(status_filepath, 'r') as status_file:
            task_status = yaml.load(status_file, Loader=yaml.FullLoader)
            logger.info(f'''Task status:\n{yaml.dump(task_status)}''')
        logger.debug(f'''\n{yaml.dump(task_status)}''')
        try:
            if task_status is None:
                error_messages.append(f"Error in module {module_name}: Status file not found.")
                logger.error('''Aborting workflow. Status file not found.''')
                error_messages.append(error_messages[-1])
                raise Exception(error_messages[-1])
            else:
                if 'code' in task_status:
                    assert task_status['code'] >= 200 and task_status['code'] < 300
                else:
                    error_messages.append(f"Error in module {module_name}: Status file does not contain a code.")
                    raise Exception(error_messages[-1])
        except AssertionError:
            logger.error(f'''Aborting workflow. Module reports failure:\n{task_status['message']}''')
            error_messages.append(f"Error in module {module_name}: Internal failure. Code: {task_status['code']}, "
                                  f"Message: {task_status['message']}")
            raise
    except SoftTimeLimitExceeded:
        logs.append(f'Time limit exceeded. Task "{self.request.id}" terminated for job "{job_id}".')
        task_status = {
            'code': 500,
            'message': 'Time limit exceeded.'
        }

    except Exception as e:
        logs.append(f'Error: {str(e)}')
        error_messages.append(str(e))
        update_job_state(job_id, Job.JobStatus.FAILURE, error_info=' \n '.join(error_messages))
        raise

    finally:
        s3.put_object(data=logs, path=os.path.join(s3_basepath, 'log.json'))

    uploaded_dirs = []
    for label, binding in bindings.items():
        if binding['type'] == 'output' and binding['host'] not in uploaded_dirs:
            s3.store_folder(
                src_dir=binding['host'],
                bucket_root_path=os.path.join(s3_basepath, binding['container'].strip('/')),
            )
            uploaded_dirs.append(binding['host'])
    # Record task as complete for monitoring
    update_workflow_state(job_id, process_names=[process_name], status='completed')
    return task_status


@shared_task(name='Workflow Complete',
             time_limit=task_time_limit,
             soft_time_limit=task_soft_time_limit)
def workflow_complete(job_id=''):
    # Capture the list of output files
    # logger.debug('Creating JobFile database records...')
    s3_basepath = os.path.join(
        settings.S3_BASE_DIR,
        f'''jobs/{job_id}''')
    paths = s3.list_directory(s3_basepath)
    for path in paths:
        file_size = s3.object_info(path).size
        # logger.debug(f'''  "{path.replace(s3_basepath, '', 1)}"''')
        JobFile.objects.create(
            job=Job.objects.get(uuid__exact=job_id),
            path=path.replace(s3_basepath, '', 1),
            size=file_size,
        )
    # Cleanup scratch files
    delete_job_scratch_files.delay(job_id)
    # Set workflow status to success
    update_job_state(job_id, Job.JobStatus.SUCCESS)


@shared_task(name='Workflow Init',
             time_limit=task_time_limit,
             soft_time_limit=task_soft_time_limit)
def workflow_init(job_id: str = '', config: dict = {}):
    while True:
        # Calculate size of /scratch to determine free space
        #   See https://pubs.opengroup.org/onlinepubs/009695399/basedefs/sys/statvfs.h.html
        #   and https://docs.python.org/3/library/os.html#os.statvfs
        statvfs = os.statvfs('/scratch')
        scratch_total = statvfs.f_frsize * statvfs.f_blocks  # Size of filesystem in bytes
        scratch_free = statvfs.f_frsize * statvfs.f_bavail  # Number of free bytes available to non-privileged process.
        free_percentage = round(100.0 * scratch_free / scratch_total)
        logger.debug(f'''Job scratch free space: {scratch_free} bytes ({free_percentage}%)''')
        # If there is sufficient free scratch space, launch the job
        # if free_percentage > settings.CE_JOB_SCRATCH_FREE_PERCENT:
        #     break
        if scratch_free > settings.CE_JOB_SCRATCH_FREE_SPACE:
            break
        else:
            logger.info(f'''Insufficient free scratch space {round(scratch_free / 1024**2)} MiB '''
                        f'''({free_percentage}%). Waiting...''')
            time.sleep(60)
    # If enough scratch space is available, mark job status as STARTED
    update_job_state(job_id, Job.JobStatus.STARTED)
    # Write workflow config to output folder
    s3_basepath = os.path.join(
        settings.S3_BASE_DIR,
        f'''jobs/{job_id}''')
    s3.put_object(data=yaml.dump(config),
                  path=os.path.join(s3_basepath, 'workflow.yaml'),
                  json_output=False)
    # Write metadata file to capture provenance info
    ce_config = load_config_from_file()
    metadata = {
        'calculation_engine': {
            'version': ce_config['version'],
        },
        'job': {
            'time': datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%dT%H:%M:%SZ"),
            'uuid': job_id,
        }
    }
    s3.put_object(data=yaml.dump(metadata),
                  path=os.path.join(s3_basepath, 'meta.yaml'),
                  json_output=False)


@shared_task(name='Delete Job Scratch Files',
             time_limit=task_time_limit,
             soft_time_limit=task_soft_time_limit)
def delete_job_scratch_files(job_id):
    logger.info(f'''Cleaning up job {job_id} scratch files...''')
    scratch_dir = os.path.join('/scratch', job_id)
    if os.path.exists(scratch_dir):
        rmtree(scratch_dir)
        logger.info(f'''Job {job_id} scratch files deleted.''')


@shared_task(name='Workflow Job Error Handler',
             time_limit=task_time_limit,
             soft_time_limit=task_soft_time_limit)
def wf_error_handler(request, exc, traceback, job_id):
    logger.error(f'''Workflow error! Celery task ID: {request.id}, job ID: {job_id}''')
    # logger.error(f'''exc: {exc}''')
    # logger.error(f'''traceback: {traceback}''')
    logger.info(f'''Cleaning up job {job_id} scratch files (in error handler)...''')
    delete_job_scratch_files(job_id)
    logger.info(f'''Job {job_id} scratch files deleted (by error handler).''')


@task_failure.connect()
def task_failed(task_id=None, exception=None, args=None, traceback=None, einfo=None, **kwargs):
    logger.error("from task_failed ==> task_id: " + str(task_id))
    logger.error("from task_failed ==> args: " + str(args))
    logger.error("from task_failed ==> exception: " + str(exception))
    logger.error("from task_failed ==> einfo: " + str(einfo))
    try:
        job_id = kwargs['kwargs']['job_id']
        job = Job.objects.get(uuid__exact=job_id)
        if not job.error_info:
            err_msg = f"System Error: {str(einfo)}"
            update_job_state(job_id, Job.JobStatus.FAILURE, error_info=err_msg)
            job.error_info = err_msg
        else:
            logger.error("from task_failed ==> job.error_info: " + str(job.error_info))
    except KeyError:
        logger.info(f"From task_failed ==> KeyError: {kwargs['kwargs']}")
        pass


@task_postrun.connect()
def task_postrun_notifier(sender=None, task_id=None, task=None, args=None, retval=None, **kwargs):
    logger.info("From task_postrun_notifier ==> sender: " + str(sender))
    result = AsyncResult(task_id)
    logger.info(f"From task_postrun_notifier ==> state: {result.state}")
    logger.info(f"From task_postrun_notifier ==> retval: {retval}")


@task_revoked.connect()
def task_revoked_notifier(sender=None, request=None, terminated=None, signum=None, expired=None, **kwargs):
    logger.info(f"From task_revoked_notifier ==> task_id: {request.id} (terminated: {terminated})")
    logger.debug(f"from task_revoked_notifier ==> sender: {sender}")
    logger.debug(f"from task_revoked_notifier ==> request: {request}")
    logger.info(f"from task_revoked_notifier ==> job_id: {request.kwargs['job_id']}")

    try:
        job_id = request.kwargs['job_id']
        job = Job.objects.get(uuid__exact=job_id)
        if not job.error_info:
            err_msg = "Job was revoked."
            update_job_state(job_id, Job.JobStatus.REVOKED, error_info=err_msg)
            job.error_info = err_msg
        else:
            logger.error("from task_revoked_notifier ==> job.error_info: " + str(job.error_info))
    except KeyError:
        logger.info(f"From task_revoked_notifier ==> KeyError: {request.kwargs}")
        pass
