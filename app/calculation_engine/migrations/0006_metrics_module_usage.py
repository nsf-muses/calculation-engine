# Generated by Django 5.1.2 on 2024-12-10 20:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('calculation_engine', '0005_job_completed_processes_job_current_processes'),
    ]

    operations = [
        migrations.AddField(
            model_name='metrics',
            name='module_usage',
            field=models.JSONField(default=dict, null=False),
        ),
    ]
