from django.contrib import admin
from calculation_engine.models import Upload, Job, Metric


class UploadAdmin(admin.ModelAdmin):
    pass


class JobsAdmin(admin.ModelAdmin):
    pass


class MetricAdmin(admin.ModelAdmin):
    pass


admin.site.register(Upload, UploadAdmin)
admin.site.register(Job, JobsAdmin)
admin.site.register(Metric, MetricAdmin)
