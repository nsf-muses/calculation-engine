from django.test import TestCase
import os
import yaml
from pathlib import Path
from calculation_engine.config import validate_config


def load_config():
    config_path = os.path.join(Path(__file__).resolve().parent, 'config.pass.yaml')
    with open(config_path, 'r') as fh:
        config_data = yaml.safe_load(fh)
    return config_data


class CalculationEngineConfigTests(TestCase):

    def test_config_actual(self):
        self.assertTrue(validate_config())

    def test_config_pass(self):
        config_path = os.path.join(Path(__file__).resolve().parent, 'config.pass.yaml')
        self.assertTrue(validate_config(config_path=config_path))

    def test_config_fail_name_1(self):
        config_data = load_config()
        config_data['modules'][0]['name'] = 'consecutive__underscores'
        self.assertFalse(validate_config(config_data=config_data))

    def test_config_fail_name_2(self):
        config_data = load_config()
        config_data['modules'][0]['name'] = '1_starts_with_number'
        self.assertFalse(validate_config(config_data=config_data))

    def test_config_fail_name_3(self):
        config_data = load_config()
        config_data['modules'][0]['name'] = 'ends_with_underscore_'
        self.assertFalse(validate_config(config_data=config_data))

    def test_config_fail_missing_property_1(self):
        config_data = load_config()
        config_data['modules'][0]['source'] = {}
        self.assertFalse(validate_config(config_data=config_data))

    def test_config_fail_wrong_type_1(self):
        config_data = load_config()
        config_data['modules'][0]['docs']['path'] = 0
        self.assertFalse(validate_config(config_data=config_data))
