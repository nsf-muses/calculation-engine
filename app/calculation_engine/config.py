import os
import json
import yaml
from app_base import settings
from openapi_core import Spec, validate_response
from openapi_core.validation.response.exceptions import InvalidData
from openapi_core.validation.exceptions import ValidationError
from openapi_core.testing import MockRequest, MockResponse
from openapi_spec_validator import validate
from openapi_spec_validator.readers import read_from_filename

from calculation_engine.log import get_logger
logger = get_logger(__name__)


def load_config_from_file():
    # Load configuration file
    def load_yaml_file(config_path):
        logger.debug(f'''Loading config file: "{config_path}"''')
        with open(config_path, "r") as conf_file:
            config = yaml.load(conf_file, Loader=yaml.FullLoader)
        return config

    # Load secret configuration
    #
    def import_secret_config(in_conf: dict, secret_conf: dict, path=[]):
        conf = dict(in_conf)
        for key in secret_conf:
            # logger.debug(f'''path: {path}, key: {key}''')
            if key in conf:
                if isinstance(conf[key], dict) and isinstance(secret_conf[key], dict):
                    # TODO: Why does path.append(str(key)) fail here? Why is path + [str(key)] necessary?
                    conf[key] = import_secret_config(conf[key], secret_conf[key], path + [str(key)])
                elif conf[key] == secret_conf[key]:
                    pass
                else:
                    logger.debug(f'''Duplicate key "{key}" with conflicting value found in secret config. '''
                                 f'''Overriding initial value with "{secret_conf[key]}"''')
                    conf[key] = secret_conf[key]
            else:
                conf[key] = secret_conf[key]
        return conf

    # If the config has already been processed, load this and return. Otherwise,
    # perform the full loading process.
    if os.path.exists(settings.CE_CONFIG_PROCESSED_PATH):
        return load_yaml_file(settings.CE_CONFIG_PROCESSED_PATH)
    else:
        logger.debug(f'Processed file not found: "{settings.CE_CONFIG_PROCESSED_PATH}".')
        config = load_yaml_file(settings.CE_CONFIG_PATH)

    secret_config = {}
    if os.path.isfile(settings.CE_CONFIG_SECRET_PATH):
        try:
            with open(settings.CE_CONFIG_SECRET_PATH, "r") as conf_file:
                secret_config = yaml.load(conf_file, Loader=yaml.FullLoader)
        except Exception as e:
            logger.error(f'''Error reading secret configuration: {e}''')
    try:
        config = import_secret_config(config, secret_config)
        # logger.debug(f'''conf: {yaml.dump(config, indent=2)}''')
    except Exception as e:
        logger.error(f'''Error importing secret configuration: {e}''')
        raise

    return config


def validate_config(config_spec_path=settings.CE_CONFIG_SPEC_PATH, config_path=settings.CE_CONFIG_PATH, config_data={}):
    # Validating config spec against OpenAPI definition
    logger.debug('Validating config spec against OpenAPI definition...')
    with open(config_spec_path, 'r') as fh:
        config_spec = Spec.from_file(fh)
    spec_dict, _ = read_from_filename(config_spec_path)
    validate(spec_dict)

    # Validating CE config data against spec
    logger.debug('Validating CE config data against spec...')
    if not config_data:
        with open(config_path, 'r') as fh:
            config_data = yaml.safe_load(fh)
    # logger.debug(config_data)
    try:
        # Validate against schema
        validate_response(
            spec=config_spec,
            request=MockRequest(host_url='/', method="GET", path='/'),
            response=MockResponse(data=json.dumps(config_data)))
    except (InvalidData, ValidationError) as err:
        logger.error('Config failed validation!')
        errors = []
        for error in err.__cause__.schema_errors:
            # Skip "None for not nullable" error as it always come double with another error, for when no value is given for a property
            if error.message == "None for not nullable":
                continue
            try:
                rel_path = ''
                while error.relative_path:
                    if rel_path:
                        rel_path += ' -> '
                    rel_path += error.relative_path.popleft()
                    rel_path += f''' [{str(error.relative_path.popleft())}]'''
                errors.append(f"Error {len(errors)+1}: {error.message} ({rel_path})")
            except Exception:
                errors.append(f"Error {len(errors)+1}: {error.message}")
        logger.error('\n'.join(errors))
        return False

    # Validate module name
    import re
    regex = re.compile(r'^[a-z][a-z0-9_]*[a-z0-9]$')
    for name in [module['name'] for module in config_data['modules']]:
        try:
            assert regex.fullmatch(name)
            assert name.find('__') == -1
        except AssertionError:
            logger.error(f'Invalid module name: {name}')
            return False

    logger.info('Valid configuration.')
    return True
