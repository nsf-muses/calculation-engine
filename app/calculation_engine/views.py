import os
from rest_framework.response import Response
from django.views.generic import ListView
from django.core.exceptions import ValidationError
from calculation_engine.models import Job, Upload, JobFile, Metric
from rest_framework import viewsets, status, renderers
from django.http import HttpResponseForbidden
from calculation_engine.serializers import JobSerializer, UploadSerializer, MetricSerializer
from rest_framework.parsers import FormParser, MultiPartParser
# from silk.profiling.profiler import silk_profile
from rest_framework.permissions import BasePermission
from calculation_engine.log import get_logger
from calculation_engine.object_store import ObjectStore
from calculation_engine.models import update_job_state
from django.http import StreamingHttpResponse
from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime, timedelta, timezone
from django.shortcuts import render
from django.conf import settings
from calculation_engine.config import load_config_from_file
from rest_framework.decorators import api_view, renderer_classes
from django.contrib.auth.decorators import login_required
from celery import chain, group
from calculation_engine.tasks_api import revoke_job, delete_job, delete_job_files
from calculation_engine.tasks import delete_job_scratch_files
from calculation_engine.workflows import launch_workflow, Workflow
from calculation_engine.limits import get_usage_limits, usage_limit_exceeded
import yaml
logger = get_logger(__name__)

s3 = ObjectStore()


class IsStaff(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_staff


class IsAdmin(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_superuser


class JobViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows jobs to be viewed or edited.
    """
    model = Job
    serializer_class = JobSerializer
    permission_classes = [IsStaff | IsAdmin]

    def get_queryset(self):
        if self.request.user.is_superuser:
            queryset = Job.objects.all()
        else:
            queryset = Job.objects.filter(owner__exact=self.request.user)
        return queryset

    def perform_create(self, serializer):
        serializer.is_valid(raise_exception=True)
        return serializer.save(owner=self.request.user)

    # @silk_profile(name='Create Job')
    def create(self, request, *args, **kwargs):
        logger.debug(f'Creating job from request: {request.data}')
        # Enforce usage limits
        exceeded, msg = usage_limit_exceeded(label='concurrent_jobs', user=self.request.user)
        if exceeded:
            return Response(
                status=status.HTTP_403_FORBIDDEN,
                data=msg)
        # Create the Job table record
        response = super().create(request, args, kwargs)
        job_id = str(response.data['uuid'])
        job_name = response.data['name']
        try:
            request_config = response.data['config']
        except Exception as err:
            logger.error(f'''Error: {err}''')
            response = Response(
                status_code=status.HTTP_400_BAD_REQUEST,
                data=f'''{err}''',
            )
            update_job_state(job_id, Job.JobStatus.FAILURE, error_info=f"Error requesting the config file: {err}")
            return response
        # Launch workflow as async Celery tasks
        logger.debug(f'''Launching Celery task for workflow "{job_name}"...''')
        try:
            # Construct and validate workflow
            wf_config = request_config['workflow']['config']
            logger.debug(f'''Workflow config:\n{yaml.dump(wf_config, indent=2)}''')
            wf = Workflow(config=wf_config, job_id=job_id)
            logger.debug(f'''Workflow object:\n{wf}''')
            # Obtain usage limit value
            limits = get_usage_limits(user=self.request.user)
            limit_value = limits['process_instances']['limit']
            if wf.num_process_instances > limit_value:
                err_msg = ('Workflow exceeds limit for the maximum number of process instances: '
                           f'{wf.num_process_instances} (max allowed: {limit_value})')
                logger.debug(err_msg)
                raise Exception(err_msg)
            # Launch workflow
            launch_workflow.apply_async(kwargs={
                'request_config': request_config,
                'job_id': job_id,
            }, task_id=job_id)
        except Exception as err:
            err_msg = f'Failed to launch workflow: {err}'
            logger.error(err_msg)
            response = Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=f'''{err_msg}''',
            )
            update_job_state(job_id, Job.JobStatus.FAILURE, error_info=err_msg)
            return response
        return response

    def destroy(self, request, pk=None, *args, **kwargs):
        job_id = pk
        response = Response(status=status.HTTP_204_NO_CONTENT)
        # If the authenticated user does not own the job, do not delete anything
        job = Job.objects.filter(uuid__exact=job_id, owner__exact=self.request.user)
        if not job:
            response.data = f'Job {job_id} not found or not job owner.'
            response.status_code = status.HTTP_404_NOT_FOUND
            return response
        logger.info(f'''Deleting job "{job_id}"...''')
        # Delete job record from database and delete job files
        # after revoking workflow tasks using async Celery tasks
        delete_chain = chain(
            revoke_job.si(job_id),
            group([
                delete_job.si(job_id),
                delete_job_files.si(job_id),
                delete_job_scratch_files.si(job_id),
            ]),
        )
        delete_chain.delay()
        return response

    def partial_update(self, request, pk=None, *args, **kwargs):
        response = Response()
        uuid = pk
        try:
            job = self.get_queryset().filter(uuid__exact=uuid)
        except ValidationError:
            response.data = 'Invalid UUID.'
            response.status_code = status.HTTP_400_BAD_REQUEST
            return response
        if not job:
            response.data = f'Job {uuid} not found or not job owner.'
            response.status_code = status.HTTP_404_NOT_FOUND
            return response
        if 'saved' in request.data and request.data['saved'] is True:
            # Enforce usage limits
            exceeded, msg = usage_limit_exceeded(label='saved_jobs', user=self.request.user)
            if exceeded:
                return Response(
                    status=status.HTTP_403_FORBIDDEN,
                    data=msg)
        response = super().partial_update(request, uuid, args, kwargs)
        return response


class JobListView(ListView):

    model = Job
    permission_classes = [IsStaff | IsAdmin]
    template_name = 'calculation_engine/job_list.html'

    def handle_no_permission(self):
        return HttpResponseForbidden(content='You must be authenticated.')

    def get_queryset(self):
        if self.request.user.is_superuser:
            queryset = super().get_queryset()
        else:
            queryset = super().get_queryset().filter(owner__exact=self.request.user)
        return queryset


class PassthroughRenderer(renderers.BaseRenderer):
    """
        Return data as-is. View should supply a Response.
    """
    media_type = ''
    format = ''

    def render(self, data, accepted_media_type=None, renderer_context=None):
        return data


class JobFileDownloadViewSet(viewsets.ViewSet):

    permission_classes = [IsStaff | IsAdmin]
    throttle_scope = 'download'

    def download(self, request, job_id=None, file_path=None, *args, **kwargs):
        job_id = str(job_id)
        file_path = file_path.strip('/')
        obj_key = os.path.join(settings.S3_BASE_DIR, 'jobs', job_id, file_path)
        file_path = os.path.join('/', file_path)
        response = Response()
        job_file = JobFile.objects.filter(job__owner__exact=self.request.user,
                                          job__uuid__exact=job_id,
                                          path__exact=file_path)
        if not job_file:
            response.data = f'File "{file_path}" not found for job {job_id}.'
            response.status_code = status.HTTP_404_NOT_FOUND
            return response
        job_file = job_file[0]
        obj_stream = s3.stream_object(obj_key)
        response = StreamingHttpResponse(streaming_content=obj_stream)
        response['Content-Disposition'] = 'attachment; filename="%s"' % os.path.basename(file_path)
        return response


class UploadDownloadViewSet(viewsets.ViewSet):

    permission_classes = [IsStaff | IsAdmin]

    def download(self, request, upload_id=None, *args, **kwargs):
        response = Response(
            status=status.HTTP_200_OK,
        )
        upload_id = str(upload_id)
        uploaded_file = Upload.objects.filter(uuid__exact=upload_id)
        try:
            uploaded_file = Upload.objects.get(uuid__exact=upload_id)
            assert uploaded_file.owner == request.user or uploaded_file.public
        except (ObjectDoesNotExist, AssertionError):
            response = Response(
                status=status.HTTP_404_NOT_FOUND,
                data=f'Upload {upload_id} not found or is private.')
            return response
        # get an open file handle
        file_handle = uploaded_file.file.open()

        # send file
        response = StreamingHttpResponse(streaming_content=file_handle)
        response['Content-Disposition'] = 'attachment; filename="%s"' % uploaded_file.file.name
        return response


class MetricViewSet(viewsets.ModelViewSet):
    serializer_class = MetricSerializer
    permission_classes = [IsStaff | IsAdmin]
    queryset = Metric.objects.all()


class UploadViewSet(viewsets.ModelViewSet):
    parser_classes = (MultiPartParser, FormParser)
    serializer_class = UploadSerializer
    permission_classes = [IsStaff | IsAdmin]
    queryset = Upload.objects.all()

    def get_queryset(self):
        queryset = self.queryset.filter(owner__exact=self.request.user)
        return queryset

    def destroy(self, request, pk=None):
        response = Response()
        uuid = pk
        if uuid:
            uploaded_file = self.get_queryset().filter(uuid__exact=uuid, owner__exact=self.request.user)
        else:
            response.data = 'uuid must be specified.'
            response.status_code = status.HTTP_400_BAD_REQUEST
            return response
        if not uploaded_file:
            response.data = f'File {uuid} not found.'
            response.status_code = status.HTTP_404_NOT_FOUND
            return response
        uploaded_file = uploaded_file[0]
        try:
            assert uploaded_file.owner == request.user
        except (ObjectDoesNotExist, AssertionError):
            response = Response(
                status=status.HTTP_403_FORBIDDEN,
                data='You can only delete uploads that you own.')
            return response
        logger.info(f'''Deleting uploaded file "{uploaded_file.path}" ("{uploaded_file.uuid}")...''')
        uploaded_file_path = os.path.join(settings.S3_BASE_DIR, 'uploads', str(uploaded_file.file))
        s3.delete_directory(uploaded_file_path)
        uploaded_file.delete()
        response.data = f'File {uploaded_file.path} deleted.'
        response.status_code = status.HTTP_200_OK
        return response

    def create(self, request):
        response = Response()
        logger.debug(request.data)
        # Enforce usage limits
        # TODO: This enforcement technically allows exceeding the size limit because it does
        #       not check the total upload size PLUS the file currently being uploaded
        exceeded, msg = usage_limit_exceeded(label='upload_size', user=self.request.user)
        if msg:
            logger.debug(f'File upload size limit exceeded: {msg}')
        if exceeded:
            return Response(
                status=status.HTTP_403_FORBIDDEN,
                data=msg)
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            response.data = serializer.errors
            logger.error(response.data)
            response.status_code = status.HTTP_400_BAD_REQUEST
            return response
        logger.debug(serializer.validated_data)
        user_path = serializer.validated_data['path']
        uploaded_file = self.get_queryset().filter(path__exact=user_path)
        if uploaded_file:
            uploaded_file = uploaded_file[0]
            response.data = f'File path "{user_path}" already exists with UUID: {uploaded_file.uuid}'
            response.status_code = status.HTTP_409_CONFLICT
            return response
        upload = Upload.objects.create(
            owner=self.request.user,
            public=serializer.validated_data['public'],
            description=serializer.validated_data['description'],
            path=user_path,
            file=serializer.validated_data['file'],
            size=serializer.validated_data['file'].size,
        )
        upload.save()
        response.data = {
            'uuid': upload.uuid,
            'path': upload.path,
            'description': upload.description,
            'public': upload.public,
            'checksum': upload.checksum,
        }
        response.status_code = status.HTTP_201_CREATED
        return response

    def partial_update(self, request, pk=None):
        uuid = pk
        response = Response(
            data=f'No changes made to file {uuid}',
            status=status.HTTP_200_OK,
        )
        # logger.debug(f'upload update(): request.data: {request.data}')
        try:
            uploaded_file = self.get_queryset().filter(uuid__exact=uuid)
        except ValidationError:
            response.data = 'Invalid UUID.'
            response.status_code = status.HTTP_400_BAD_REQUEST
            return response
        if not uploaded_file:
            response.data = f'File {uuid} not found.'
            response.status_code = status.HTTP_404_NOT_FOUND
            return response
        uploaded_file = uploaded_file.filter(owner__exact=request.user)
        if not uploaded_file:
            response.data = f'You do not own upload {uuid}.'
            response.status_code = status.HTTP_403_FORBIDDEN
            return response
        uploaded_file = uploaded_file[0]
        if 'public' in request.data:
            uploaded_file.public = request.data['public']
            uploaded_file.save()
            response.data = f'File {uuid} updated.'
        if 'description' in request.data:
            uploaded_file.description = request.data['description']
            uploaded_file.save()
            response.data = f'File {uuid} updated.'
        return response

    # def list(self, request, *args, **kwargs):
    #     # response = super().list(request, args, kwargs)
    #     response = Response()
    #     # Only return a list of uploads owned by the user; do not include all possible public uploads owned by others
    #     response.data = self.get_queryset().filter(owner__exact=self.request.user)
    #     return response

    def retrieve(self, request, pk=None, *args, **kwargs):
        response = Response()
        uuid = pk
        try:
            upload = Upload.objects.get(uuid__exact=uuid)
            assert upload.public or upload.owner == request.user
            serializer = self.get_serializer(upload)
            response = Response(serializer.data)
        except (ObjectDoesNotExist, AssertionError):
            response = Response(
                status=status.HTTP_404_NOT_FOUND,
                data=f'Upload {uuid} not found or is private.')
        return response


class UploadListView(ListView):

    model = Upload
    permission_classes = [IsStaff | IsAdmin]
    template_name = 'calculation_engine/upload_list.html'

    def handle_no_permission(self):
        return HttpResponseForbidden(content='You must be authenticated.')

    def get_queryset(self):
        if self.request.user.is_superuser:
            queryset = super().get_queryset()
        else:
            queryset = super().get_queryset().filter(owner__exact=self.request.user)
        return queryset

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, args, kwargs)
        return response

    def delete(self, request, *args, **kwargs):
        response = super().delete(request, args, kwargs)
        return response


class MetricListView(ListView):

    model = Metric
    permission_classes = [IsStaff | IsAdmin]
    template_name = 'calculation_engine/metrics.html'

    def get_queryset(self):
        time_threshold = datetime.now(timezone.utc) - timedelta(hours=24)
        queryset = super().get_queryset().filter(time_collected__gt=time_threshold)
        for obj in queryset:
            obj.job_files_size_units = 'bytes'
            if obj.job_files_size > 1024**2:
                obj.job_files_size_units = 'MiB'
                obj.job_files_size = round(obj.job_files_size / 1024**2, 0)
            elif obj.job_files_size > 1024**3:
                obj.job_files_size_units = 'GiB'
                obj.job_files_size = round(obj.job_files_size / 1024**3, 0)
        return queryset

    def handle_no_permission(self):
        return HttpResponseForbidden(content='You must be authenticated.')

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, args, kwargs)
        return response


@api_view(('GET',))
@renderer_classes((renderers.JSONRenderer,))
def ModuleListView(request):
    config = load_config_from_file()
    if request.content_type == 'application/json':
        return Response(data=config['modules'])
    modules = []
    for module in config['modules']:
        try:
            docs_source_home = module['docs']['home']
        except KeyError:
            docs_source_home = ''
        docs_source_url = f'''{module['source']['url']}/tree/{module['source']['targetRevision']}'''
        docs_source_url += f'''/{module['docs']['path']}'''
        if docs_source_home:
            docs_source_url += f'''/{module['docs']['home']}'''
        image_url = f'''{os.path.join(module['image']['registry'], module['image']['repo'])}:{module['image']['tag']}'''
        modules.append({
            'name': module['name'],
            'version': module['source']['targetRevision'],
            'repository_url': module['source']['url'],
            'docs_source_url': docs_source_url,
            'docs_source_home': docs_source_home,
            'image_url': image_url,
        })

    context = {
        'title': 'Modules',
        'url': settings.HOSTNAMES[0],
        'modules': modules,
        'version': config['version'],
    }
    return render(request, "calculation_engine/modules.html", context)


@api_view(('GET',))
@login_required
@renderer_classes((renderers.JSONRenderer,))
def UsageLimitListView(request):
    # config = load_config_from_file()
    # if request.content_type == 'application/json':
    #     return Response(data=config['modules'])

    context = {
        'title': 'Limits',
        'url': settings.HOSTNAMES[0],
        'limits': get_usage_limits(user=request.user),
    }
    return render(request, "calculation_engine/limits.html", context)
