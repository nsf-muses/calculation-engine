# Publishing Releases

The MUSES Calculation Engine will continue to evolve. There will be bug fixes and updates to registered modules, among other changes. The release version system is important not only for researchers who need to be aware of how CE updates impact their custom-built client software, it is also important how it supports the proper citation of the CE when researchers publish results involving output from CE workflows.

The challenge for the CE developers is that the CE version must exist in multiple files to be rendered or parsed by a variety of consumers. For example, the Django web application needs to know the version so it can be displayed on the web interface and so that metadata can be output with workflow files for provenance tracking. We want to prominently display the text people should use to cite the CE, including code snippets (like BibTeX) they can copy-paste. Another example is [the `CITATION.cff` file](https://citation-file-format.github.io/), which is consumed by services like Zenodo, GitHub, Zotero to automatically generate bibliographic information to display.

There is a utility script `scripts/make_release.py` that eases the process of creating a new release and publishing it to Zenodo using Zenodo's programmatic API. The order of operations is outlined below.

1. Create Zenodo record draft for target release version v1.2.3 and obtain the new prereserved DOI.
1. Update the relevant source files with the version number 1.2.3 and the DOI.
1. Commit the changes and tag the commit v1.2.3.
1. Create an archive of v1.2.3 by 
   1. locally cloning the repo to `/tmp` (to ensure a pristine copy that only includes files in the Git index),
   1. checking out tag v1.2.3, and
   1. making a zip file.
1. Upload the archive file to the draft Zenodo record.
1. Push the new version to production and publish the Zenodo record manually in the web interface after carefully inspecting the record.

Before running the script, ensure that your Git working directory is pristine by checking with `git status --porcelain` and that you are on the `main` branch.

Generate a Zenodo personal access token with scopes `deposit:actions, deposit:write, user:email` and export it in your shell session as `ZENODO_ACCESS_TOKEN`. Then run the script as shown below. Optionally, use the `testing` option to test how files will change without actually creating a tag or uploading an archive file.

```bash
$ export ZENODO_ACCESS_TOKEN=FBh...Eh

$ python scripts/make_release.py 0.9.3 [testing]

Downloading existing Zenodo record info into "/var/tmp/zenodo_record.0.9.3.json".
Creating tagged release "v0.9.3"...
Items staged for commit: ['CITATION.cff', 'config/config.yaml']
Committing to main branch...
Tagging commit "v0.9.3"...
...
```
