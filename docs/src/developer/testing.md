# Testing

We use the [Django test framework](https://docs.djangoproject.com/en/5.1/topics/testing/overview/) for unit tests and workflow tests. One of the many advantages of this framework is that the environment is reset to a fiducial state for each test to avoid hysteretic effects.

## Workflows

To run a test workflow, first launch the CE in development mode:

```bash
$ bash docker/launch.sh dev up
```

Once online, open a terminal in the API server container:

```bash
$ docker exec -it ce-dev-api-server-1 bash
```

Run the tests using the Django `manage.py test` command syntax. To run a single module workflow, you can use a command like the one below:

```bash
ce@08a7d755568a:/opt/app$ python manage.py test \
                          calculation_engine.tests.test_workflows.WorkflowTests.test_crust_dft
```

## Job cannon

The job cannon is a script that uses Python multiprocessing to launch and monitor many jobs in parallel. With an instance of the CE running, use the `python test/job_cannon.py [num_replicas] [workflow_list]` command to launch one or more copies of a set of workflows.

```bash
$ source .venv/bin/activate

$ # Run all tests
$ python test/job_cannon.py 1 all

$ # Run one or more tests by name
$ python test/job_cannon.py 1 cmf cmf_lepton eos_taylor_4d

[14:54:01] Process started: test_cmf-1
[14:54:01] Process started: test_eos_taylor_4d-1
[14:54:01] Process started: test_cmf_lepton-1
[14:54:16] Process finished: test_cmf-1
[14:54:16] Process finished: test_eos_taylor_4d-1
[14:54:21] Process finished: test_cmf_lepton-1

$ python -c 'import test.job_cannon as cn; cn.list_jobs();'

34 jobs total
26 failed jobs
8 successful jobs
0 incomplete jobs
created  first: 2024-12-17T21:43:51.537302Z
created  last:  2024-12-18T16:28:02.864086Z
modified first: 2024-12-17T21:43:52.076595Z
modified last:  2024-12-18T16:28:11.059952Z

```
