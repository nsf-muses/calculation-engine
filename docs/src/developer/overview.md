# Overview

This section of the documentation includes topics of interest to people interested in developing components of the MUSES CI, including the individual calculation modules, the core Calculation Engine (CE), and the deployment infrastructure.
