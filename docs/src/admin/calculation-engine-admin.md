# Calculation Engine Administration

The Calculation Engine (CE) is the application that integrates the MUSES modules by providing an API for running scientific calculation workflows involving one or more modules. This document contains information relevant to system administrators.

## Manual job deletion

Jobs can be deleted via the command line using the low-level Django model API if necessary. This
is preferable to direct SQL database commands because there are linked model objects in Django,
such as the owner record. Also, there are job files that need to be removed.

Open a terminal in one of the API server replicas:

```bash
$ export KUBECONFIG=/home/andrew/.kube/nsf-muses.kubeconfig

$ kubectl exec -it -n ce-dev api-server-0 -c app -- bash

ce@api-server-0:/opt/app$ python manage.py shell
Python 3.11.9 (main, Sep  4 2024, 23:15:21) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
```

Then execute a script similar to this one, where in this example, jobs older than 10 days
are deleted.

```python
import yaml
import datetime
from models import Job
from calculation_engine.tasks import delete_job_files

jobs = Job.objects.filter(created__lt=time_threshold)
time_threshold = datetime.datetime.utcnow() - datetime.timedelta(days=10)
jobs = Job.objects.filter(created__lt=time_threshold)
print(yaml.dump([{'created': job.created, 'uuid': str(job.uuid)} for job in jobs]))
for job in jobs:
    delete_job_files.delay(job.uuid)
    job.delete()
```
