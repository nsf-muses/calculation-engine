How to cite the MUSES Calculation Engine
========================================

If you use the MUSES Calculation Engine to generate data that
contributes to a research publication, either via the public online
service or locally on your own machines, you should cite the software as
you would scientific publications.

The metadata needed to generate the citation can be found in the
``CITATION.cff`` file in the root of the source code repo at |CitationFileUrl|.
There are `several tools available to convert between CFF and other formats 
<https://citation-file-format.github.io/#/tools-for-working-with-citationcff-files>`__.

For convenience, you can download a :download:`BibTeX citation file here<bibtex.bib>`.

