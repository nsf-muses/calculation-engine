{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "YrwFNLIi98qk"
   },
   "source": [
    "# MUSES Calculation Engine Tutorial\n",
    "\n",
    "## Introduction\n",
    "\n",
    "This notebook will show you how to use the MUSES Calculation Engine (CE). It is designed to for each cell to be manually executed after reading and following the instructions so that you understand the purpose of each step. When you complete the tutorial, you will be ready to develop your own Python modules to aid your research.\n",
    "\n",
    "While this tutorial relies heavily on the `CalculationEngineApi` class to provide a Python interface to the RESTful API, you are encouraged to read the technical API specs at https://ce.musesframework.io/swagger-ui, where you can also experiment with the API interactively after logging in at https://ce.musesframework.io."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "traftzQLD_p3"
   },
   "outputs": [],
   "source": [
    "import os\n",
    "import yaml\n",
    "import logging\n",
    "import getpass\n",
    "import time\n",
    "\n",
    "from calculation_engine_api import CalculationEngineApi"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Configure connection settings\n",
    "\n",
    "🛑 **STOP! Before you proceed, please follow the steps in the [Quick Start guide](https://musesframework.io/docs/user/quickstart.html) to ensure that you have a MUSES account and that you are properly logged into [the Calculation Engine](https://ce.musesframework.io).**\n",
    "\n",
    "Press the green **API Token** button on the [Calculation Engine web app](https://ce.musesframework.io/) and enter your token at the prompt.\n",
    "\n",
    "It should look like `ae9e48be14efbaabcdd90b44a3505b94d52b2c25`.\n",
    "\n",
    "🛈 If you are running locally in Docker Compose, you do not need to provide a token because the local admin account will be used instead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "traftzQLD_p3"
   },
   "outputs": [],
   "source": [
    "# Set environment variables\n",
    "if os.getenv('RUNNING_IN_DOCKER', '') == 'true':\n",
    "    os.environ['CE_API_URL_PROTOCOL'] = 'http'\n",
    "    os.environ['CE_API_URL_AUTHORITY'] = 'api-proxy:4000'\n",
    "else:\n",
    "    os.environ['LOG_LEVEL'] = str(logging.INFO)\n",
    "    os.environ['CE_API_URL_PROTOCOL'] = 'https'\n",
    "    os.environ['CE_API_URL_AUTHORITY'] = 'ce.musesframework.io'\n",
    "    os.environ['CE_API_TOKEN'] = getpass.getpass()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Obtain an API object to interact with the Calculation Engine"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "traftzQLD_p3"
   },
   "outputs": [],
   "source": [
    "api = CalculationEngineApi()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Workflow example 1: CMF (Chiral Mean Field)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define your workflow"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "FWCvqsc6gnnM",
    "outputId": "e992a3db-42c9-451d-ae8e-ea06753580b1"
   },
   "outputs": [],
   "source": [
    "wf_config = yaml.safe_load('''\n",
    "    processes:\n",
    "      - name: cmf\n",
    "        module: cmf_solver\n",
    "        config:\n",
    "          variables:\n",
    "            chemical_optical_potentials:\n",
    "              muB_begin: 1000.0\n",
    "              muB_end: 1400.0\n",
    "              muB_step: 200.0\n",
    "              use_hyperons: false\n",
    "              use_decuplet: false\n",
    "              use_quarks: false\n",
    "    components:\n",
    "      - type: group\n",
    "        name: run_cmf_test\n",
    "        group:\n",
    "          - cmf\n",
    "''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Launch a job to run the workflow"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "job_response = api.job_create(\n",
    "    description='Tutorial job',\n",
    "    name= 'Tutorial. CMF',\n",
    "    config={\n",
    "    \"workflow\": {\n",
    "        \"config\": wf_config\n",
    "    }\n",
    "})\n",
    "try:\n",
    "    job_id = job_response['uuid']\n",
    "except:\n",
    "    print(f'''HTTP {job_response.status_code} {job_response.text}''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Query the status of the job"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('The job has ID ', job_id)\n",
    "\n",
    "while True:\n",
    "    job_status = api.job_list(uuid=job_id)\n",
    "    print('\\tStatus: ', job_status['status'])\n",
    "  \n",
    "    if job_status['status'] in ['SUCCESS', 'FAILURE']:\n",
    "        break  # Exit the loop if the job is done\n",
    "\n",
    "    time.sleep(10)  # Adjust the check delay as necessary\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Download the job output files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "FWCvqsc6gnnM",
    "outputId": "e992a3db-42c9-451d-ae8e-ea06753580b1"
   },
   "outputs": [],
   "source": [
    "output_dir='./downloads/jobs/'\n",
    "job_info = api.job_list(uuid=job_id)\n",
    "print(f'''Downloading job output files. Saving into {os.path.join(output_dir,job_id)}''')\n",
    "for file_info in job_info['files']:\n",
    "    if file_info['size'] > 0:\n",
    "        print(f'''  \"{file_info['path']}\"...''')\n",
    "        api.download_job_file(job_id, file_info['path'], root_dir=output_dir)\n",
    "    else:\n",
    "        print(f'''  WARNING: Skipping zero-length file \"{file_info['path']}\"''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Save a job and make it available for others to use\n",
    "\n",
    "Jobs expire after some time to conserve our storage capacity and ensure fair use of the system. You can save a limited number of jobs to prevent them from being automatically deleted. Additionally, you can mark jobs as public so that other may use the output as inputs to their workflows.\n",
    "\n",
    "The example below shows how you can save a job and make it public in a single API call (`PATCH /api/v0/ce/job/{id}/`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Fetch the current job state\n",
    "job_info = api.job_list(uuid=job_id)\n",
    "saved = \"NOT \" if not job_info['saved'] else ''\n",
    "public = \"NOT \" if not job_info['public'] else ''\n",
    "print(f'''(before update) Job {job_id} is {public}public and {saved}saved.''')\n",
    "\n",
    "# Set the job public and saved state to the opposite of whatever it is currently\n",
    "api.update_job(job_id, saved=not job_info['saved'], public=not job_info['public'])\n",
    "job_info = api.job_list(uuid=job_id)\n",
    "saved = \"NOT \" if not job_info['saved'] else ''\n",
    "public = \"NOT \" if not job_info['public'] else ''\n",
    "print(f'''(after update) Job {job_id} is {public}public and {saved}saved.''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## **Workflow example 2: Crust DFT → Lepton → QLIMR**\n",
    "\n",
    "In this section you will learn how to run a series of modules sequentially. Each module will use the output generated by the preceding module, creating a streamlined workflow that generates and transforms data.\n",
    "\n",
    "We will use the Crust DFT (Density Functional Theory) module to generate a 2-dimensional Equation of S                                                                                                                                                                                      tate (EoS) based on baryon density and charge fraction ($n_B$, $y_Q$). The EoS will be passed to the Lepton module, which will compute the $\\beta$-equilibrated EoS (1-dimensional). Finally, the equilibrated EoS is piped to the QLIMR module, to calculate the mass-radius curve of a Neutron Star. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define your workflow\n",
    "\n",
    "Here we define a new workflow configuration, specifying the modules involved in the `processes` section and the structure of the workflow in the `components` section.\n",
    " \n",
    "Check [the documentation for individual modules](https://musesframework.io/docs/) for more details."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wf_config = yaml.safe_load('''\n",
    "processes:\n",
    "- name: crust_dft_eos\n",
    "  module: crust_dft\n",
    "  inputs:\n",
    "    EOS_table:\n",
    "      type: upload\n",
    "      uuid: d1ed1c63-6192-4ac9-9cb1-a7d82cc27b72\n",
    "      checksum: 164575f9d84c3ac087780e0219ee2e8a\n",
    "  config:\n",
    "    output_format: CSV\n",
    "    generate_table: false\n",
    "    ext_guess: false\n",
    "    set:\n",
    "      Ye_grid_spec: 70,0.01*(i+1)\n",
    "      nB_grid_spec: 301,10^(i*0.04-12)*2.0\n",
    "      inc_lepton: false\n",
    "- name: lepton-crust_dft\n",
    "  module: lepton\n",
    "  config:\n",
    "    global:\n",
    "      use_beta_equilibrium: true\n",
    "      use_charge_neutrality: false\n",
    "    output:\n",
    "      output_derivatives: true\n",
    "    particles:\n",
    "      use_electron: true\n",
    "      use_muon: true\n",
    "  pipes:\n",
    "    input_eos:\n",
    "      label: crust_dft_output\n",
    "      module: crust_dft\n",
    "      process: crust_dft_eos\n",
    "- name: qlimr-crust_dft\n",
    "  module: qlimr\n",
    "  pipes:\n",
    "    eos:\n",
    "      label: eos_beta_equilibrium\n",
    "      module: lepton\n",
    "      process: lepton-crust_dft\n",
    "  config:\n",
    "    inputs:\n",
    "      R_start: 0.0004\n",
    "      eos_name: eos\n",
    "      final_epsilon: 6000.0\n",
    "      initial_epsilon: 250.0\n",
    "      resolution_in_NS_M: 0.05\n",
    "      resolution_in_NS_R: 0.2\n",
    "      single_epsilon: 700.0\n",
    "    options:\n",
    "      eps_sequence: true\n",
    "      output_format: csv\n",
    "      stable_branch: true\n",
    "    outputs:\n",
    "      compute_inertia: false\n",
    "      compute_love: false\n",
    "      compute_mass_and_radius_correction: false\n",
    "      compute_quadrupole: false\n",
    "      local_functions: false\n",
    "components:\n",
    "- type: chain\n",
    "  name: crust_dft-lepton\n",
    "  sequence:\n",
    "  - crust_dft_eos\n",
    "  - lepton-crust_dft\n",
    "  - qlimr-crust_dft\n",
    "''')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Launch a job to run the workflow"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "job_response = api.job_create(\n",
    "    description='Tutorial job',\n",
    "    name='Tutorial. DFT - Lepton - QLIMR',\n",
    "    config={\n",
    "    \"workflow\": {\n",
    "        \"config\": wf_config\n",
    "    }\n",
    "})\n",
    "try:\n",
    "    job_id = job_response['uuid']\n",
    "except:\n",
    "    print(f'''HTTP {job_response.status_code} {job_response.text}''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Query the status of the job with automatic polling"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('The job has ID ', job_id)\n",
    "\n",
    "while True:\n",
    "    job_status = api.job_list(uuid=job_id)\n",
    "    print('\\tStatus: ', job_status['status'])\n",
    "  \n",
    "    if job_status['status'] in ['SUCCESS', 'FAILURE']:\n",
    "        break  # Exit the loop if the job is done\n",
    "\n",
    "    time.sleep(15)  # Adjust the check delay as necessary"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Download the job output files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "output_dir='./downloads/jobs/'\n",
    "job_status = api.job_list(uuid=job_id)\n",
    "\n",
    "print('''Downloading job output files.\n",
    "Saving into ''', output_dir+job_id)\n",
    "\n",
    "for file_info in job_status['files']:\n",
    "    print(f'''  \"{file_info['path']}\"...''')\n",
    "    if file_info['size'] > 0:\n",
    "        api.download_job_file(job_id, file_info['path'], root_dir=output_dir)\n",
    "    else:\n",
    "        print(f'''  Skipping zero-length file.''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Workflow Example 3a: Upload a file and use it in a workflow\n",
    "\n",
    "Now we will upload the $\\beta$-equilibrated EoS for the Crust DFT model and run the QLIMR module with it, as in Example 2.\n",
    "- *Make sure you've run all Example 2 cells before!*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define a help function to get job by name"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_job_by_name(name):\n",
    "    search = [job for job in api.job_list() if job['name'] == name]\n",
    "    if search:\n",
    "        return search[0]\n",
    "    else:\n",
    "        return None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### List all uploads"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uploads = api.upload_list()\n",
    "print(yaml.dump({'uploads': [{\n",
    "    'path': upload['path'],\n",
    "    'description': upload['description'],\n",
    "    'size': f'''{round(upload['size'] / 1024**2)} MiB''',\n",
    "    'checksum': upload['checksum'], \n",
    "    'uuid': upload['uuid'],  \n",
    "} for upload in uploads]}, sort_keys=False))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set the upload path"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "upload_path = '/crust_dft/eos.csv'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Delete the uploaded file if it already exists"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uploads = api.upload_list()\n",
    "for upload in uploads:\n",
    "    uuid = upload['uuid']\n",
    "    path = upload['path']\n",
    "    if path == upload_path:\n",
    "        print(f'''Deleting upload \"{path}\" ({uuid})''')\n",
    "        api.delete_uploaded_file(uuid=uuid)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Upload the Equation of State"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "job = get_job_by_name(name='Tutorial. DFT - Lepton - QLIMR')\n",
    "job_id = job['uuid']\n",
    "crust_dft_upload= api.upload_file(file_path= f'./downloads/jobs/{job_id}/lepton-crust_dft/opt/output/beta_equilibrium_eos.csv',\n",
    "                              upload_path=upload_path,\n",
    "                              public=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define a QLIMR configuration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wf_config = yaml.safe_load(f'''\n",
    "processes:\n",
    "- name: qlimr-crust_dft\n",
    "  module: qlimr\n",
    "  inputs:\n",
    "    eos:\n",
    "      type: upload\n",
    "      uuid: {crust_dft_upload['uuid']}\n",
    "      checksum: {crust_dft_upload['checksum']}\n",
    "  config:\n",
    "    inputs:\n",
    "      R_start: 0.0004\n",
    "      eos_name: eos\n",
    "      final_epsilon: 6000.0\n",
    "      initial_epsilon: 250.0\n",
    "      resolution_in_NS_M: 0.05\n",
    "      resolution_in_NS_R: 0.2\n",
    "      single_epsilon: 700.0\n",
    "    options:\n",
    "      eps_sequence: true\n",
    "      output_format: csv\n",
    "      stable_branch: true\n",
    "    outputs:\n",
    "      compute_inertia: false\n",
    "      compute_love: false\n",
    "      compute_mass_and_radius_correction: false\n",
    "      compute_quadrupole: false\n",
    "      local_functions: false\n",
    "components:\n",
    "- type: chain\n",
    "  name: crust_dft-qlimr\n",
    "  sequence:\n",
    "  - qlimr-crust_dft\n",
    "''')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Launch the job"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "job_response = api.job_create(\n",
    "    description='Tutorial job',\n",
    "    name= 'Tutorial. DFT upload - QLIMR',\n",
    "    config={\n",
    "    \"workflow\": {\n",
    "        \"config\": wf_config\n",
    "    }\n",
    "})\n",
    "try:\n",
    "    job_id = job_response['uuid']\n",
    "except:\n",
    "    print(f'''HTTP {job_response.status_code} {job_response.text}''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Query the status of the job"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('The job has ID ', job_id)\n",
    "\n",
    "while True:\n",
    "    job_status = api.job_list(uuid=job_id)\n",
    "    print('\\tStatus: ', job_status['status'])\n",
    "  \n",
    "    if job_status['status'] in ['SUCCESS', 'FAILURE']:\n",
    "        break\n",
    "\n",
    "    time.sleep(5) # Adjust the check delay as necessary\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Download the job output files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "output_dir='./downloads/jobs/'\n",
    "job_status = api.job_list(uuid=job_id)\n",
    "\n",
    "print('''Downloading job output files.\n",
    "Saving into ''', output_dir+job_id)\n",
    "\n",
    "for file_info in job_status['files']:\n",
    "    print(f'''  \"{file_info['path']}\"...''')\n",
    "    if file_info['size'] > 0:\n",
    "        api.download_job_file(job_id, file_info['path'], root_dir=output_dir)\n",
    "    else:\n",
    "        print(f'''  Skipping zero-length file.''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Delete the uploaded file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "api.delete_uploaded_file(uuid=crust_dft_upload['uuid'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Workflow Example 3b: Use previous job output directly as an input file\n",
    "\n",
    "Now we will run the same workflow as we did in example 3a, but this time instead of downloading and uploading the input file we need, we will specify the input file directly from the previous job.\n",
    "\n",
    "- *Make sure you've run all Example 2 cells before!*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fetch the job by name"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "job = get_job_by_name(name='Tutorial. DFT - Lepton - QLIMR')    \n",
    "input_job_id = job['uuid']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define the workflow using the previous job output EoS for QLIMR input"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wf_config = yaml.safe_load(f'''\n",
    "processes:\n",
    "- name: qlimr-crust_dft\n",
    "  module: qlimr\n",
    "  inputs:\n",
    "    eos:\n",
    "      type: job\n",
    "      uuid: {input_job_id}\n",
    "      path: /lepton-crust_dft/opt/output/beta_equilibrium_eos.csv\n",
    "  config:\n",
    "    inputs:\n",
    "      R_start: 0.0004\n",
    "      eos_name: eos\n",
    "      final_epsilon: 6000.0\n",
    "      initial_epsilon: 250.0\n",
    "      resolution_in_NS_M: 0.05\n",
    "      resolution_in_NS_R: 0.2\n",
    "      single_epsilon: 700.0\n",
    "    options:\n",
    "      eps_sequence: true\n",
    "      output_format: csv\n",
    "      stable_branch: true\n",
    "    outputs:\n",
    "      compute_inertia: false\n",
    "      compute_love: false\n",
    "      compute_mass_and_radius_correction: false\n",
    "      compute_quadrupole: false\n",
    "      local_functions: false\n",
    "components:\n",
    "- type: chain\n",
    "  name: crust_dft-qlimr\n",
    "  sequence:\n",
    "  - qlimr-crust_dft\n",
    "''')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wf_config['processes'][0]['inputs']['eos']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Launch the job"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "job_response = api.job_create(\n",
    "    description='Tutorial job',\n",
    "    name= 'Tutorial. DFT saved_job - QLIMR',\n",
    "    config={\n",
    "    \"workflow\": {\n",
    "        \"config\": wf_config\n",
    "    }\n",
    "})\n",
    "try:\n",
    "    job_id = job_response['uuid']\n",
    "except:\n",
    "    print(f'''HTTP {job_response.status_code} {job_response.text}''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Query the status of the job"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('The job has ID ', job_id)\n",
    "\n",
    "while True:\n",
    "    job_status = api.job_list(uuid=job_id)\n",
    "    print('\\tStatus: ', job_status['status'])\n",
    "  \n",
    "    if job_status['status'] in ['SUCCESS', 'FAILURE']:\n",
    "        break\n",
    "\n",
    "    time.sleep(5) # Adjust the check delay as necessary\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Download the job output files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "output_dir='./downloads/jobs/'\n",
    "job_status = api.job_list(uuid=job_id)\n",
    "\n",
    "print('''Downloading job output files.\n",
    "Saving into ''', output_dir+job_id)\n",
    "\n",
    "for file_info in job_status['files']:\n",
    "    print(f'''  \"{file_info['path']}\"...''')\n",
    "    if file_info['size'] > 0:\n",
    "        api.download_job_file(job_id, file_info['path'], root_dir=output_dir)\n",
    "    else:\n",
    "        print(f'''  Skipping zero-length file.''')"
   ]
  }
 ],
 "metadata": {
  "colab": {
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
