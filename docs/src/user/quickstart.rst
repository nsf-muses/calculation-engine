Quick Start Guide
=================

First steps
-----------

1. Login
~~~~~~~~

.. important::

   Please read these instructions carefully to create (or use your existing) MUSES account properly. 

   First, `log in to our community forum using this link <https://forum.musesframework.io/my/preferences/account>`__. 
   This is where you will engage with the MUSES community and seek help when you have questions.
   The forum login button will take you to a CILogon screen where you can choose which 
   identity provider (IdP) to use for authentication. **Once you have chosen your IdP you must always use
   the same one when logging in to MUSES web services**. If you are already logged in and cannot recall which
   IdP you originally used, look at your username and email address `in your Account preferences <https://forum.musesframework.io/my/preferences/account>`__;
   if you log out and back in, you should see the same username and email address.
   
   Once you are logged into the forum and are satisfied with your selected IdP, open `the Calculation Engine <https://ce.musesframework.io>`__ 
   in the same web browser. When you click the login button there you should be immediately redirected to the 
   CE home page as an authenticated user where you should see your name in the nav bar on the top-right of the screen.

   `Click here to learn more about MUSES accounts <https://forum.musesframework.io/t/muses-accounts/214>`__.

2. Request Access
~~~~~~~~~~~~~~~~~

If you see the red “Request Access” button in the navigation bar, it
means you do not yet have permission to use the CE. Click it to request
access and follow the instructions. When one of our staff approves your
request, you will receive a confirmation email. Note that this may take
hours or days depending on staff availability. If you are concerned
about your request and wish to contact someone, please `create a post on
our support
forum <https://forum.musesframework.io/new-topic?category=calc-engine-support&title=New%20user%20access%20request&body=Provide%20a%20detailed%20description%20of%20your%20problem%20and%20your%20question>`__.

3. Fetch an API token
~~~~~~~~~~~~~~~~~~~~~

After approval, log out and back in to the CE. You should now see a
green “API token” button. Press it to reveal your API access token.
**This token is only for you to use and must be kept secret.** To
invalidate a token and generate a fresh one, log out and back in.

4. Follow the tutorial
~~~~~~~~~~~~~~~~~~~~~~

We recommend that you :doc:`try our MUSES tutorial notebook </user/tutorial/Readme>` to learn 
the basics of building and launching workflows.

Additional resources and links
------------------------------

To learn more details about constructing workflows, see :doc:`/user/workflows`.

The low-level Calculation Engine `API is documented in an interactive webpage here <https://ce.musesframework.io/swagger-ui/>`__,
with `the full technical specification available here in OpenAPI format <https://ce.musesframework.io/openapi/>`__. You will need 
these specs to build your own API wrapper libraries. For inspiration, please see `this Python class <https://gitlab.com/nsf-muses/calculation-engine/-/blob/main/docs/src/user/tutorial/calculation_engine_api.py>`__ used in the tutorial.

A wealth of documentation and information about MUSES services and how
to use them is found on the `MUSES
forum <https://forum.musesframework.io>`__. There you can use the search
tool to find answers and also post your own questions to seek help from
the MUSES community.

