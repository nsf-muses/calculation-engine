# Documentation

The documentation system for the MUSES framework is based on Sphinx, where top-level documentation is contained in this git repo and registered MUSES module documentation is compiled and published at https://ce.musesframework.io/docs/.

## Module documentation

Individual modules must compose documentation in Sphinx-compatible files within some folder in their module source code repo. The module `manifest.yaml` file must include a specification `docs.path` that tells the docs compiler where to find the files as shown in the example below:

```
docs:
  path: "docs/src"
```

A working example of module documentation can be found in [`/modules/test-module-1`](https://gitlab.com/nsf-muses/calculation-engine/-/blob/main/modules/test-module-1) within this repo.
