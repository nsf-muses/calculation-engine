# Persistent Volumes for Storage

Persistent storage for services deployed in our Kubernetes cluster is provided by two dynamic storage volume provisioners:

- Longhorn
- NFS

These correspond to PersistentVolume (PV) storage classes: `longhorn` and `nfs-condo` (default).

## NFS Condo

Data connected by `nfs-condo` PVs is stored remotely on a dedicated storage cluster at NCSA called Condo. Volumes are mounted via NFS to the host nodes dynamically by the [NFS Subdirectory External Provisioner](https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/) installed via the `nfs-subdir-external-provisioner` Helm chart.

Files can be accessed by using a standard NFS mount command:

```bash
mount taiga-nfs.ncsa.illinois.edu:/taiga/ncsa/radiant/[project_code] /mnt/condo
```

## Longhorn

The `longhorn` storage class is provided by the [Longhorn](https://rancher.com/products/longhorn/) system, which allocates space from the k8s nodes themselves (i.e. flash storage attached to the host VMs) to provision the PVs claimed by the k8s pods. This storage is fast (suitable for MySQL databases for example) and is replicated across several nodes for resilience. However, if all the nodes are destroyed, then the data is lost. Backups to the Condo system can be configured in the Longhorn management interface by specifying a target NFS URL like `nfs://taiga-nfs.ncsa.illinois.edu:/taiga/ncsa/radiant/[project_code]/backup`
